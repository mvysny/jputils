/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import com.github.mvysny.dynatest.DynaTest
import sk.baka.aedict.dict.DictCode
import sk.baka.aedict.dict.JMDictEntry
import sk.baka.aedict.kanji.IRomanization
import kotlin.test.expect

class InflectorTest : DynaTest({
    test("adj inflection") {
        val e = JMDictEntry.synthetic(IRomanization.HEPBURN.toHiragana("takai"), "me", "jmdict", DictCode.adji)
        expect(true) { e.isSynthetic }
        expect(true) { e.jmDictEntry!!.isInflectableAdjective() }
        expect("[takai], [takai desu], [takaku nai], [takaku nai desu, takaku arimasen], [takakatta], [takakatta desu], [takaku nakatta], [takaku nakatta desu, takaku arimasen desita]") {
            e.jmDictEntry!!.getInflectableBase()!!.getAdjectiveInflections().values.joinToString()
        }
    }

    test("ichidan verb inflection") {
        val e = JMDictEntry.synthetic(IRomanization.HEPBURN.toHiragana("taberu"), "me", "jmdict", DictCode.v1)
        expect(true) { e.isSynthetic }
        expect(true) { e.jmDictEntry!!.isInflectableVerb() }
        expect("tabe, tabe, taberu, tabere, tabeyou, tabete, tabeta") {
            e.jmDictEntry!!.getInflectableBase()!!.getVerbBaseInflections().joinToString { it.second }
        }
        expect("taberu, tabemasu, tabemasen, tabemasita, tabemasen desita, tabetai, tabemasyou, tabenasai, tabe ni iku, tabe ni kuru, tabenikui, tabeyasui, tabesugiru, tabenagara, tabenai, tabenai desyou, tabenakatta, tabenakereba, tabenakereba narimasen, tabesaseru, taberareru, tabesaserareru, tabezu ni, taberu desyou, taberu hazu, taberu hou ga ii, taberu ka dou ka, taberu kamo siremasen, taberu kara, taberu keredomo / kedo, taberu koto ga dekiru, taberu koto ni suru, taberu made, taberu na!, taberu nara, taberu, taberu no ni, taberu no ha, taberu node, taberu noni, taberu tokoro datta, taberu sou desu, taberu tame ni, taberu ni, taberu to omou, taberu tumori, taberu you desu, tabereba, tabereba ii, tabereba yokatta, taberareru, tabeyou, tabeyou ka na, tabeyou to suru, tabete, tabete kudasai, tabete ageru, tabete goran, tabete iru, tabete itadaku, tabete morau, tabete kara, tabete kureru, tabete kuru, tabete iku, tabete miru, tabete mo, tabete mo ii, tabete bakari, tabete oku, tabete simau, tabete ha ikaga / dou desu ka, tabete ha ikemasen, tabete, tabeta, tabeta bakari, tabeta koto ga aru, tabetara, tabeta rasii, tabetari, tabeta to sitara, tabeta to site mo, tabeta toki, tabeta tokoro, taberun da") {
            e.jmDictEntry!!.getInflectableBase()!!.getVerbFormInflections().values.joinToString()
        }
    }

    test("godan verb inflection") {
        val e = JMDictEntry.synthetic(IRomanization.HEPBURN.toHiragana("asobu"), "me", "jmdict", DictCode.v5)
        expect(true) { e.isSynthetic }
        expect(true) { e.jmDictEntry!!.isInflectableVerb() }
        expect("asoba, asobi, asobu, asobe, asobou, asonde, asonda") {
            e.jmDictEntry!!.getInflectableBase()!!.getVerbBaseInflections().joinToString { it.second }
        }
        expect("asobu, asobimasu, asobimasen, asobimasita, asobimasen desita, asobitai, asobimasyou, asobinasai, asobi ni iku, asobi ni kuru, asobinikui, asobiyasui, asobisugiru, asobinagara, asobanai, asobanai desyou, asobanakatta, asobanakereba, asobanakereba narimasen, asobaseru, asobareru, asobaserareru, asobazu ni, asobu desyou, asobu hazu, asobu hou ga ii, asobu ka dou ka, asobu kamo siremasen, asobu kara, asobu keredomo / kedo, asobu koto ga dekiru, asobu koto ni suru, asobu made, asobu na!, asobu nara, asobu, asobu no ni, asobu no ha, asobu node, asobu noni, asobu tokoro datta, asobu sou desu, asobu tame ni, asobu ni, asobu to omou, asobu tumori, asobu you desu, asobeba, asobeba ii, asobeba yokatta, asobe, asoberu, asobou, asobou ka na, asobou to suru, asonde, asonde kudasai, asonde ageru, asonde goran, asonde iru, asonde itadaku, asonde morau, asonde kara, asonde kureru, asonde kuru, asonde iku, asonde miru, asonde mo, asonde mo ii, asonde bakari, asonde oku, asonde simau, asonde ha ikaga / dou desu ka, asonde ha ikemasen, asonde, asonda, asonda bakari, asonda koto ga aru, asondara, asonda rasii, asondari, asonda to sitara, asonda to site mo, asonda toki, asonda tokoro, asobun da") {
            e.jmDictEntry!!.getInflectableBase()!!.getVerbFormInflections().values.joinToString()
        }
    }
})
