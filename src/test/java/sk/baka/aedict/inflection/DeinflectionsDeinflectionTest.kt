/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.cloneBySerialization
import org.junit.jupiter.api.Test
import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.util.Boxables
import kotlin.test.expect

/**
 * @author mvy
 */
class DeinflectionsDeinflectionTest : DynaTest({
    test("box") {
        var d = VerbDeinflections.Deinflection("foo", VerbInflection.Form.ABLE_TO_DO2,
                mapOf("a" to InflectableWordClass.VERB_GODAN, "b" to InflectableWordClass.VERB_GODAN, "c" to InflectableWordClass.VERB_ICHIDAN))
        expect(d) { Boxables.testclone(d) }
        d = VerbDeinflections.Deinflection("foo", null,
                mapOf("a" to InflectableWordClass.VERB_GODAN, "b" to InflectableWordClass.VERB_GODAN, "c" to InflectableWordClass.VERB_ICHIDAN))
        expect(d) { Boxables.testclone(d) }
    }

    group("serializable") {
        test("VerbDeinflections.Deinflection") {
            val d = VerbDeinflections.Deinflection("foo", VerbInflection.Form.ABLE_TO_DO2,
                    mapOf("a" to InflectableWordClass.VERB_GODAN, "b" to InflectableWordClass.VERB_GODAN, "c" to InflectableWordClass.VERB_ICHIDAN))
            expect(d) { d.cloneBySerialization() }
        }

        test("Deinflections") {
            val d: Deinflections = Deinflector(null).deinflect("tabereru", IRomanization.NIHON_SHIKI)
            expect(d) { d.cloneBySerialization() }
        }
    }
})
