/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import com.github.mvysny.dynatest.expectList
import org.junit.jupiter.api.Test
import sk.baka.aedict.decodeBase64
import java.io.ByteArrayInputStream
import kotlin.test.expect

/**
 * @author mvy
 */
class UtilKtTest {

    @Test
    fun splitToWords() {
		expectList() { "      ".splitToWords() }
		expectList() { "    -  ".splitToWords() }
		expectList("this", "is", "a", "test", "[", "bla", "]", "+", "5", "huhu", ".") { "   this    is a test[ bla]+5 huhu.".splitToWords(true) }
		expectList("this", "is", "a", "test", "bla", "5", "huhu") { "   this    is a test[ bla]+5 huhu.".splitToWords() }
		expectList("私", "は", "彼", "女", "を", "か", "わ", "い", "ら", "し", "い", "と", "思", "っ", "た", "。") { "私は彼女をかわいらしいと思った。".splitToWords(true) }
		expectList("私", "は", "彼", "女", "を", "か", "わ", "い", "ら", "し", "い", "と", "思", "っ", "た") { "私は彼女をかわいらしいと思った。".splitToWords() }
		expectList("4", "-", "1", "-", "4") { "4-1-4".splitToWords(true) }
		expectList("4", "1", "4") { "4-1-4".splitToWords() }
	}

	@Test
	fun splitByWhitespaces() {
		expectList() { "      ".splitByWhitespaces() }
		expectList("-") { "    -  ".splitByWhitespaces() }
		expectList("this", "is", "a", "test[", "bla]+5", "huhu.") { "   this    is a test[ bla]+5 huhu.".splitByWhitespaces() }
		expectList("私は彼女をかわいらしいと思った。") { "私は彼女をかわいらしいと思った。".splitByWhitespaces() }
		// https://github.com/mvysny/aedict/issues/658
		expectList("a", "a") { String(charArrayOf('a', 160.toChar(), 'a')).splitByWhitespaces() }
		expectList("：", "iPhone、Xperiaほか、SIMロックフリー端末に使えるSIMカードをラインアップ。") { "77yawqBpUGhvbmXjgIFYcGVyaWHjgbvjgYvjgIFTSU3jg63jg4Pjgq/jg5Xjg6rjg7znq6/mnKvjgavkvb/jgYjjgotTSU3jgqvjg7zjg4njgpLjg6njgqTjg7PjgqLjg4Pjg5fjgII=".decodeBase64().splitByWhitespaces() }
		expectList("a", "apple", "adam") { "a    apple  adam".splitByWhitespaces() }
		expectList("a", "b", "c") { "    a b c    ".splitByWhitespaces() }
		expectList() { "\u3000".splitByWhitespaces() }
		// https://fabric.io/baka/android/apps/sk.baka.aedict3/issues/596e599bbe077a4dccc24fa7
		expectList("不", "忠実") { "不\u200B忠実".splitByWhitespaces() }
	}

	@Test
	fun codePointIteratorTest() {
		expect("自分自身") { "自分自身".codePoints.map { it.toChar() }.joinToString("") }
	}

	@Test
	fun testComputeSize() {
		expect(1000) { ByteArrayInputStream(ByteArray(1000)).computeSize() }
	}

	@Test
	fun testIntersects() {
		// empty sets have no elements in common
		expect(false) { setOf<String>().intersects(setOf<String>()) }
		expect(false) { setOf<String>().intersects(setOf("foo")) }
		expect(false) { setOf("foo").intersects(setOf<String>()) }
		expect(false) { setOf("foo").intersects(setOf("bar")) }
		expect(true) { setOf("foo").intersects(setOf("foo", "bar")) }
	}
}
