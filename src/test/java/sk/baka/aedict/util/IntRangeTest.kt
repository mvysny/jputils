/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import com.github.mvysny.dynatest.expectList
import org.junit.jupiter.api.Test
import kotlin.test.expect

/**
 * @author mvy
 */
class IntRangeTest {
    @Test
    fun testEmptyIntersection() {
        expect(IntRange.EMPTY) { (5..5).intersection(10..10) }
		expect(IntRange.EMPTY) { IntRange.EMPTY.intersection(10..10) }
		expect(IntRange.EMPTY) { IntRange.EMPTY.intersection(0..0) }
		expect(IntRange.EMPTY) { (0..0).intersection(IntRange.EMPTY) }
		expect(IntRange.EMPTY) { (0..0).intersection(1..1) }
		expect(IntRange.EMPTY) { (0..10).intersection(11..20) }
    }

    @Test
    fun testNonEmptyIntersection() {
        expect(0..0) { (0..0).intersection(0..0) }
        expect(0..0) { (0..0).intersection(0..10) }
        expect(10..10) { (0..10).intersection(10..12) }
        expect(2..5) { (0..10).intersection(2..5) }
        expect(2..10) { (0..10).intersection(2..25) }
        expect(2..10) { (2..25).intersection(0..10) }
    }

    @Test
    fun processEmptySet() {
        expectList() { mutableListOf<IntRange>().simplifyRanges().toList() }
        expectList() { mutableListOf(IntRange.EMPTY, 50..49).simplifyRanges().toList() }
    }

    @Test
    fun processSet() {
        expectList(0..4, 50..50) { mutableListOf(0..0, 1..1, 2..2, 1..3, 4..4, 50..50).simplifyRanges().toList() }
    }

    @Test
    fun contains() {
        expect(true) { (5..5).contains(5..5) }
        expect(true) { (5..5).contains(IntRange.EMPTY) }
        expect(false) { (5..5).contains(6..6) }
        expect(false) { (5..7).contains(6..8) }
        expect(true) { (5..7).contains(5..6) }
        expect(true) { (5..7).contains(6..7) }
        expect(true) { (5..7).contains(6..6) }
    }
}
