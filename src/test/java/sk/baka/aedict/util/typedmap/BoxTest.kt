/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap

import com.github.mvysny.dynatest.expectList
import org.junit.jupiter.api.Assertions.assertArrayEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.DictCode
import sk.baka.aedict.dict.JMDictEntry
import sk.baka.aedict.util.Boxable
import sk.baka.aedict.util.Timing
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.BitSet
import java.util.*
import kotlin.test.expect

/**
 * @author mvy
 */
class BoxTest {
    /**
     * This is to avoid accidental hashcode collisions.
     */
    @Test
    fun multiplePutFails() {
        assertThrows(IllegalStateException::class.java) {
            Box().putByte("f1", 25.toByte()).putByte("f1", 25.toByte())
        }
    }

    @Test
    fun multiplePutFails2() {
        assertThrows(IllegalStateException::class.java) {
            Box().putByte("f1", 25.toByte()).putString("f1", "25")
        }
    }

    @Test
    fun testClassCastException() {
        assertThrows(IllegalStateException::class.java) {
            Box().putInt("bla", 1).get("bla").shortValue()
        }
    }

    @Test
    fun boxEquals() {
        val box1 = Box().putBoolean("f1", true).putInt("f2", 2)
        val box2 = Box().putBoolean("f1", true).putInt("f2", 2)
        expect(box1) { box2 }
    }

    @Test
    fun serializationTest() {
        val boxable = JMDictEntry.KanjiData("kanji", true, EnumSet.noneOf(DictCode::class.java), 25, 50, null, 10.toByte())
        val uuid = UUID.randomUUID()
        val box = Box()
                .putByte("f1", 25.toByte())
                .putInt("f2", 255)
                .putShort("haha", 22.toShort())
                .putString("bla", "foobar")
                .putBoolean("asdasd", true)
                .putListOfBasics("caca", ArrayList(Arrays.asList("1", "a", "%", "cicina")))
                .putEnumSet("enums", EnumSet.of(DictCode.v1, DictCode.adjno, DictCode.baseb))
                .putBoxable("box", boxable)
                .putListOfBoxables("kvak", ArrayList(Arrays.asList<Boxable>(boxable, boxable)))
                .putByteArray("blaaaa", ByteArray(16))
                .putFloat("fufu", 1.25f)
                .putListOfBasics("asjdya4", ArrayList(setOf("foo")))  // check that Sets are handled correctly as well.
                .putEnum("enum", DictCode.ek)
                .putLong("long", 122132L)
            .putUUID("uuid", uuid)
        val box2 = Writables.clone(box)!!
        // boxes are not necessary equal since e.g. Set changes to List after deserialization
        expect(box2) { box }
        expect(25.toByte()) { box2.get("f1").byteValue().get() }
        expect(255) { box2.get("f2").integer().get() }
        expect(22.toShort()) { box2.get("haha").shortValue().get() }
        expect("foobar") { box2.get("bla").string().get() }
        expect(true) { box2.get("asdasd").bool().get() }
        expectList("1", "a", "%", "cicina") { box2.get("caca").arrayOf(String::class.java).get().toList() }
        expect(EnumSet.of(DictCode.v1, DictCode.adjno, DictCode.baseb)) { box2.get("enums").enumSet(DictCode::class.java).get() }
        expect(boxable) { box2.get("box").boxable(JMDictEntry.KanjiData::class.java).get() }
        expectList(boxable, boxable) { box2.get("kvak").arrayOf(JMDictEntry.KanjiData::class.java).get().toList() }
        assertArrayEquals(ByteArray(16), box2.get("blaaaa").byteArray().get())
        expect(122132L) { box2.get("long").longValue().get() }
        expect(1.25f) { box2.get("fufu").floatValue().get() }
        assertArrayEquals(arrayOf("foo"), box2.get("asjdya4").arrayOf(String::class.java).get())
        expect(DictCode.ek) { box2.get("enum").enumValue(DictCode::class.java).get() }
        expect(uuid) { box2.get("uuid").uuid().get() }
    }

    @Test
    fun nullEnumTest() {
        val box = Box()
        expect(null) { box.get("enum").enumValue(DictCode::class.java).orNull() }
        box.putEnum("enum", null)
        expect(null) { box.get("enum").enumValue(DictCode::class.java).orNull() }
    }

    @Test
    fun perfTest() {
        val list = ArrayList<String>()
        for (i in 0..999) {
            list.add("asdkjd902834902384erqwakisdjakjasldjasda" + i)
        }
        val box = Box().putListOfBasics("hu", list)
        val t = Timing("bla")
        for (i in 0..999) {
            Writables.clone(box)
        }
        println(t.finish())
    }

    @Test
    fun putSupportedObjectTest() {
        val boxable = JMDictEntry.KanjiData("kanji", true, EnumSet.noneOf(DictCode::class.java), 10, 15, EnumSet.of(
                JMDictEntry.Pri.Nf), 25.toByte())
        val box = Box()
                .putSupportedObject("f1", 25.toByte())
                .putSupportedObject("f2", 255)
                .putSupportedObject("haha", 22.toShort())
                .putSupportedObject("bla", "foobar")
                .putSupportedObject("asdasd", true)
                .putSupportedObject("caca", ArrayList(Arrays.asList("1", "a", "%", "cicina")))
                .putSupportedObject("enums", EnumSet.of(DictCode.v1, DictCode.adjno, DictCode.baseb))
                .putSupportedObject("box", boxable)
                .putSupportedObject("kvak", ArrayList(Arrays.asList<Boxable>(boxable, boxable)))
                .putSupportedObject("blaaaa", ByteArray(16))
                .putSupportedObject("fufu", 1.25f)
                .putSupportedObject("asjdya4", ArrayList(setOf("foo")))  // check that Sets are handled correctly as well.
                .putBooleanArray("ba", booleanArrayOf(true, false, false, false, true, false))
                .putSupportedObject("bitset", BitSet.valueOf(byteArrayOf(1, 2)))
                .putSupportedObject("long", 122132L)
        val box2 = Writables.clone(box)!!
        // boxes are not necessary equal since e.g. Set changes to List after deserialization
        expect(box2) { box }
        expect(25.toByte()) { box2.get("f1").byteValue().get() }
        expect(255) { box2.get("f2").integer().get() }
        expect(22.toShort()) { box2.get("haha").shortValue().get() }
        expect("foobar") { box2.get("bla").string().get() }
        expect(true) { box2.get("asdasd").bool().get() }
        assertArrayEquals(arrayOf("1", "a", "%", "cicina"), box2.get("caca").arrayOf(String::class.java).get())
        expect(EnumSet.of(DictCode.v1, DictCode.adjno, DictCode.baseb)) { box2.get("enums").enumSet(DictCode::class.java).get() }
        expect(boxable) { box2.get("box").boxable(JMDictEntry.KanjiData::class.java).get() }
        assertArrayEquals(arrayOf<Boxable>(boxable, boxable), box2.get("kvak").arrayOf(JMDictEntry.KanjiData::class.java).get())
        assertArrayEquals(ByteArray(16), box2.get("blaaaa").byteArray().get())
        expect(122132L) { box2.get("long").longValue().get() }
        expect(1.25f) { box2.get("fufu").floatValue().get() }
        assertArrayEquals(arrayOf("foo"), box2.get("asjdya4").arrayOf(String::class.java).get())
        expect(BitSet.valueOf(byteArrayOf(1, 2))) { box2.get("bitset").bitSet().get() }
    }

    @Test
    fun putListOfBasics() {
        var box: Box? = Box().putListOfBasics("foo", Arrays.asList(1, 25L, true, null, "foo"))
        box = Writables.clone(box)
        val list = box!!.get("foo").listOf(Any::class.java).get()
        expect(5) { list.size }
        expect(1) { list[0] }
        expect(25L) { list[1] }
        expect(true) { list[2] }
        expect(null) { list[3] }
        expect("foo") { list[4] }
    }

    @Test
    fun putSetOfBasics() {
        val box = Box().putListOfBasics("foo", HashSet<Any>(Arrays.asList(1, 25L, true, null, "foo")))
        val set = box.get("foo").setOf(Any::class.java).get()
        expect(5) { set.size }
    }

    @Test
    fun putMapOfBasics() {
        var map: MutableMap<Any, Any?> = HashMap()
        map.put("kvak", "bla")
        map.put(25, 46L)
        map.put(false, null)
        var box: Box? = Box().putMapOfBasics("map", map)
        box = Writables.clone(box)
        map = box!!.get("map").mapOfBasics<Any, Any>().get()
        expect(3) { map.size }
        expect("bla") { map["kvak"] }
        expect(46L) { map[25] }
        expect(null) { map[false] }
        expect(true) { map.keys.contains(false) }
    }

    enum class TestEnum {
        A,
        B,
        C
    }

    @Test
    fun putEnumMapOfBasics() {
        var map = EnumMap<TestEnum, Any>(TestEnum::class.java)
        map.put(TestEnum.A, "bla")
        map.put(TestEnum.B, 46L)
        map.put(TestEnum.C, null)
        var box: Box? = Box().putMapOfBasics("map", map)
        box = Writables.clone(box)
        map = box!!.get("map").enumMapOfBasics<TestEnum, Any>(TestEnum::class.java).get()
        expect(3) { map.size }
        expect("bla") { map[TestEnum.A] }
        expect(46L) { map[TestEnum.B] }
        expect(null) { map[TestEnum.C] }
        expect(true) { map.keys.contains(TestEnum.C) }
    }

    @Test
    fun putBooleanArray() {
        val b = booleanArrayOf(false, true, true, false, false, false, true, false, false)
        var box: Box = Box().putBooleanArray("hu", b).putBooleanArray("hu2", null)
        box = Writables.clone(box)!!
        assertArrayEquals(b, box.get("hu").booleanArray().get())
        expect(null) { box.get("hu2").booleanArray().orNull() }
    }
}
