/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import kotlin.test.expect

class DictTest {
    @Test
    fun testGlob() {
        expect(true) { "*a*".globToPattern().matches("aaa") }
        expect(true) { "*a*".globToPattern().matches("a") }
        expect(true) { "*a*".globToPattern().matches("bbabb") }
        expect(true) { "*a?b*".globToPattern().matches("bbabb") }
        expect(false) { "a?b".globToPattern().matches("bbabb") }
        expect(true) { "a?b".globToPattern().matches("abb") }
        expect(true) { "?a?b".globToPattern().matches("tabb") }
        expect(true) { "ta*ru".globToPattern().matches("taberu") }
        expect(false) { "ta?ru*".globToPattern().matches("taberu") }
    }
}
