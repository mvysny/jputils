/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import com.github.mvysny.dynatest.DynaTest
import com.github.mvysny.dynatest.expectMap
import sk.baka.aedict.kanji.mapKatakanaToHiragana
import sk.baka.aedict.kanji.toKanji
import kotlin.test.expect
import kotlin.test.fail

class RubyFuriganaTest : DynaTest({
    test("mapKanjiToReadingTest") {
        val rf = RubyFurigana("見た目", "みため")
        expectMap("見" to "み", "目" to "め") { rf.mapKanjiToReading(true).mapKeys { it.key.kanji() } }
        expectMap("見" to "み", "目" to "め") { rf.mapKanjiToReading(false).mapKeys { it.key.kanji() } }
    }

    test("mapKanjiToReadingTest3") {
        val rf = RubyFurigana("見猿た目", "みざため")
        expectMap("見" to "み", "猿" to "ざ", "目" to "め") { rf.mapKanjiToReading(true).mapKeys { it.key.kanji() } }
        expectMap("見" to "み", "猿" to "ざ", "目" to "め") { rf.mapKanjiToReading(false).mapKeys { it.key.kanji() } }
    }

    test("mapKanjiToReadingTest2") {
        val rf = RubyFurigana("見猿", "みざる")
        // nesmie to namapovat takto: 見->miza 猿->ru lebo potom do indexu ide toto: W見るW Wみざ猿W W見猿W WみざるW (permutovanie readingov)
        // a potom search na 見る najde aj toto.
        expectMap("見" to "み", "猿" to "ざる") { rf.mapKanjiToReading(false).mapKeys { it.key.kanji() } }
        // ale na druhej strane, sentence analyza vyzera ist horsie, lebo napr. vetu
        // リズムがおもしろかったり音楽がよかったりしますね。
        // zanalyzuje tak, ze miesto slova ongaku 音楽 zoberie RI a najde 流音: りゅうおん
        expectMap("見" to "みざ", "猿" to "る") { rf.mapKanjiToReading(true).mapKeys { it.key.kanji() } }
    }

    test("furiganaViewForm") {
        var rf = RubyFurigana("見た目", "みため")
        expect("{見;み}た{目;め}") { rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) }
        expect("見[み]た目[め]") { rf.format(RubyFurigana.FORMATTER_ANKI) }
        expect("<ruby><rb>見</rb><rp>(</rp><rt>み</rt><rp>)</rp></ruby>た<ruby><rb>目</rb><rp>(</rp><rt>め</rt><rp>)</rp></ruby>") { rf.format(RubyFurigana.FORMATTER_HTML) }
        rf = RubyFurigana("見", "みため")
        expect("{見;みため}") { rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) }
        expect("見[みため]") { rf.format(RubyFurigana.FORMATTER_ANKI) }
        rf = RubyFurigana("見目", "みため")
        expect("{見目;みため}") { rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) }
        expect("見目[みため]") { rf.format(RubyFurigana.FORMATTER_ANKI) }
    }

    test("mapJibunjishin") {
        val rf = RubyFurigana("自分自身", "じぶんじしん")
        expectMap("自" to "じ", "分" to "ぶん", "身" to "しん") { rf.mapKanjiToReading(true).mapKeys { it.key.kanji() } }
        expect("{自分自身;じぶんじしん}") { rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) }
        expect("<ruby><rb>自分自身</rb><rp>(</rp><rt>じぶんじしん</rt><rp>)</rp></ruby>") { rf.format(RubyFurigana.FORMATTER_HTML) }
    }

    test("testBullshitInput") {
        val rf = RubyFurigana("huhu[bla", "caca")
        expect("{huhu[bla;caca}") { rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) }
        expect("huhu[bla[caca]") { rf.format(RubyFurigana.FORMATTER_ANKI) }
    }

    test("testOneKanjiMoreReadings") {
        val rf = RubyFurigana("御目出度う御座います", "おめでとうございます")
        expectMap("御" to "ご", "目" to "め", "出" to "で", "度" to "と", "座" to "ざ") {
            rf.mapKanjiToReading(false).mapKeys { it.key.kanji() }
        }
        expectMap("御" to "ご", "目" to "め", "出" to "で", "度" to "と", "座" to "ざ") {
            rf.mapKanjiToReading(true).mapKeys { it.key.kanji() }
        }
        expect("御目出度[ごめでと]う御座[ござ]います") { rf.format(RubyFurigana.FORMATTER_ANKI) }
    }

    /**
     * Unfortunately this is why https://github.com/mvysny/aedict/issues/693 cannot be fully fixed.
     */
    test("testIncorrectGuess") {
        val rf = RubyFurigana("郵政省", "ゆうせいしょう")
        expectMap("郵" to "ゆ", "政" to "う", "省" to "せいしょう") {
            rf.mapKanjiToReading(false).mapKeys { it.key.kanji() }
        }
        // skus vsak povedat, ze 政 musi mat reading しょう; mapKanjiToReading musi vratit prazdnu mnozinu lebo sa neda ostatnym kanji
        // v tomto pripade priradit reading
        expectMap() {
            rf.mapKanjiToReading(false, mapOf('政'.toKanji() to "しょう")).mapKeys { it.key.kanji() }
        }
    }

    test("performance") {
        val rf = RubyFurigana("最近の銀行員の態度はなんと慇懃無礼なことか。何様だと思ってるんだ。",
                "さいきんの ぎんこういん の たいどはなんと いんぎんぶれいな こと か。なにさま だ と おもってる んだ。")
        rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW) // warmup
        // this took 2 seconds on a powerful machine. It must run under 150 millis
        val start = System.currentTimeMillis()
        repeat(10) {
            expect("{最近;さいきん}の{銀行員;ぎんこういん}の{態度;たいど}はなんと{慇懃無礼;いんぎんぶれい}なことか。{何様;なにさま}だと{思;おも}ってるんだ。") {
                // this was slow as hell: https://github.com/mvysny/aedict/issues/775
                rf.format(RubyFurigana.FORMATTER_FURIGANA_VIEW)
            }
        }
        val duration = System.currentTimeMillis() - start
        expect(true, "Too long: $duration") { duration < 150 }
    }

    group("mapKanjiToReadingExact2") {
        test("basic kunyomi matching") {
            val ta = Kanjidic2Entry('食'.toKanji(), 1, null, null, listOf("ショク", "ジキ"), listOf("く.う", "く.らう", "た.べる", "は.む"),
                    Gloss(), "", listOf("ぐい"), listOf(), null, null, null, null, null, null, setOf())
            val taberu = RubyFurigana("食べる", "たべる")
            expect("{食;た}べる") { taberu.toFuriganaExact(mapOf('食'.toKanji() to ta))!!.format() }
        }
        test("advanced onyomi matching") {
            val mappings = mapOf('吸'.toKanji() to listOf("キュウ".mapKatakanaToHiragana(), "す"),
                    '収'.toKanji() to listOf("シュウ".mapKatakanaToHiragana(), "おさ", "おさ", "のぶ"))
            expect("{吸;きゅう}{収;しゅう}") { RubyFurigana("吸収", "きゅうしゅう").toFuriganaExact2(mappings, false)!!.format() }
            expect("{吸;す}う") { RubyFurigana("吸う", "すう").toFuriganaExact2(mappings, false)!!.format() }
        }
        test("mismatching reading test") {
            expect(mapOf()) { RubyFurigana("ＡＢＣＤ包", "エービーシーディーほう").mapKanjiToReadingExact2(mapOf('包'.toKanji() to listOf("ほう"))) }
        }
        test("different readings") {
            expect(mapOf()) { RubyFurigana("包包", "ほう").mapKanjiToReadingExact2(mapOf('包'.toKanji() to listOf("ほ", "う"))) }
        }
    }
})
