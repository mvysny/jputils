/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import com.github.mvysny.dynatest.DynaTest
import org.junit.jupiter.api.Test
import sk.baka.aedict.kanji.Heisig
import sk.baka.aedict.kanji.Kanji
import sk.baka.aedict.kanji.getUniqueKanjis
import sk.baka.aedict.util.Language
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.cloneByBoxing
import java.util.*
import kotlin.test.expect

class Kanjidic2EntryTest : DynaTest({
    test("serialization") {
        expect(full) { Writables.clone(full) }
        expect(full) { full.cloneByBoxing() }
    }

    test("serializationNull") {
        val g = Gloss()
        g.add(Language.ENGLISH, "ha")
        val e = Kanjidic2Entry(
                Kanji('鬯'),
                5,
                null,
                null,
                ArrayList(Arrays.asList("bla", "blu")),
                ArrayList(Arrays.asList("quak")),
                g,
                "",
                ArrayList(),
                ArrayList<String>(),
                null,
                null,
                null,
                null,
                null,
                null,
                setOf())
        expect(e) { Writables.clone(e) }
        expect(e) { e.cloneByBoxing() }
    }

    test("getKanjis") {
        expect(setOf(Kanji('鬯'))) { full.getAllKanjis() }
    }

    test("sense one liner") {
        expect("ha, foo") { full.getSenseOneLiner(null, false) }
        expect("ha, foo") { full.getSenseOneLiner(null, true) }
    }

    test("isMocked") {
        expect(false) { full.isMocked }
        expect(true) { Kanjidic2Entry.mock(Kanji('鬯')).isMocked }
        expect("Not a regular Japanese kanji") { Kanjidic2Entry.mock(Kanji('鬯')).getSenseOneLiner(null, false) }
    }
}) {
    companion object {
        val full = Kanjidic2Entry(
                Kanji('鬯'),
                5,
                SkipCode(3, 5, 6),
                7,
                ArrayList(Arrays.asList("bla", "blu")),
                ArrayList(Arrays.asList("quak")),
                Gloss().add(Language.ENGLISH, "ha").add(Language.ENGLISH, "foo"),
                "a",
                ArrayList<String>(),
                ArrayList(Arrays.asList("kvak")),
                Heisig(Gloss().add(Language.ENGLISH, "kvak"), 5.toShort(), 3.toByte(), 10),
                EnumMap<Kanjidic2Entry.DicRef, Any>(Collections.singletonMap(Kanjidic2Entry.DicRef.crowley, 25)),
                "bla1",
                "bla2",
                "bla3",
                10,
                "鬯".getUniqueKanjis())
    }
}
