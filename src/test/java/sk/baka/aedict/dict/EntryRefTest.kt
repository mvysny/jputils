/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.Language
import sk.baka.aedict.util.Writables
import sk.baka.aedict.util.cloneByBoxing
import java.util.*
import kotlin.test.expect

/**
 * @author mvy
 */
class EntryRefTest {
    @Test
    fun testWritable() {
        expect(EntryRef.of(ExamplesEntryTest.bare, "bla")) { Writables.clone(EntryRef.of(ExamplesEntryTest.bare, "bla")) }
        expect(EntryRef.of(Kanjidic2EntryTest.full)) { Writables.clone(EntryRef.of(Kanjidic2EntryTest.full)) }
        expect(EntryRef.of(JMDictEntryTest.jmdict, "bla", true)) { Writables.clone(EntryRef.of(JMDictEntryTest.jmdict, "bla", true)) }
    }

    @Test
    fun testBoxable() {
        expect(EntryRef.of(ExamplesEntryTest.bare, "bla")) { EntryRef.of(ExamplesEntryTest.bare, "bla").cloneByBoxing() }
        expect(EntryRef.of(Kanjidic2EntryTest.full)) { EntryRef.of(Kanjidic2EntryTest.full).cloneByBoxing() }
        expect(EntryRef.of(JMDictEntryTest.jmdict, "bla", true)) { EntryRef.of(JMDictEntryTest.jmdict, "bla", true).cloneByBoxing() }
    }

    @Test
    fun testMatchesWithSelf() {
        expect(true) { EntryRef.of(JMDictEntryTest.jmdict, "bla", true).matches(EntryRef.of(JMDictEntryTest.jmdict, "bla", true)) }
    }

    /**
     * Test for https://github.com/mvysny/aedict/issues/488
     */
    @Test
    fun test方KataHouDoesNotMatch() {
        val kata = JMDictEntry(listOf(JMDictEntry.KanjiData("方", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.ReadingData("kata", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"),
                        emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        val hou = JMDictEntry(listOf(JMDictEntry.KanjiData("方", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.ReadingData("hou", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"),
                        emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        expect(false) { EntryRef.of(kata, "bla", true).matches(EntryRef.of(hou, "bla", true)) }
    }

    /**
     * Test for https://github.com/mvysny/aedict/issues/488
     */
    @Test
    fun testAddedReadingDoesMatch() {
        val kata = JMDictEntry(listOf(JMDictEntry.KanjiData("kanji", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.ReadingData("kata", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"), emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        val hou = JMDictEntry(listOf(JMDictEntry.KanjiData("kanji", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                Arrays.asList(
                        JMDictEntry.ReadingData("hou", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte()),
                        JMDictEntry.ReadingData("kata", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"), emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        expect(true) { EntryRef.of(kata, "bla", true).matches(EntryRef.of(hou, "bla", true)) }
        // ale nematchnu, ak su z roznych slovnikov.
        expect(false) { EntryRef.of(kata, "bla2", true).matches(EntryRef.of(hou, "bla", true)) }
    }

    /**
     * Test for https://github.com/mvysny/aedict/issues/488
     */
    @Test
    fun testHibiAkagireDoesNotMatch() {
        val crack = JMDictEntry(listOf(JMDictEntry.KanjiData("k1", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                Arrays.asList(
                        JMDictEntry.ReadingData("hibi", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte()),
                        JMDictEntry.ReadingData("akagire", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"), emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        val crack2 = JMDictEntry(listOf(JMDictEntry.KanjiData("hibi", false, EnumSet.noneOf(DictCode::class.java), 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.ReadingData("hibi", null, EnumSet.noneOf(DictCode::class.java), false, 1, 1, null, (-1).toByte())),
                listOf(JMDictEntry.Sense(null, null, null, null, null, Gloss().add(Language.ENGLISH, "bla"), emptyList<JMDictEntry.LoanWord>(), null, emptyList<String>(), emptyList<String>())),
                2f, JLPTLevel.N5)
        expect(false) { EntryRef.of(crack, "bla", true).matches(EntryRef.of(crack2, "bla", true)) }
    }
}
