package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class DictCodeTest {
    @Test
    fun lookupByXml() {
        assertEquals(DictCode.rK, DictCode.fromJmdictXml("rarely used kanji form"))
        assertEquals(DictCode.rK, DictCode.fromJmdictXml("rarely-used kanji form"))
    }
}