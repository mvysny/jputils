/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.download.DictionaryMeta
import kotlin.test.assertNotEquals
import kotlin.test.expect

/**
 * @author mvy
 */
class DictionaryIDTest {

    @Test
    fun testToString() {
        expect("JMDICT") { DictionaryMeta.DictionaryID.EDICT_JB.toString() }
        expect("EXAMPLE_SENTENCES/kotowaza") { DictionaryMeta.DictionaryID.KOTOWAZA.toString() }
    }

    @Test
    fun testEquals() {
        expect(DictionaryMeta.DictionaryID(DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES, null)) { DictionaryMeta.DictionaryID(DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES, null) }
        assertNotEquals(DictionaryMeta.DictionaryID(DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES, null), DictionaryMeta.DictionaryID(DictionaryMeta.DictionaryClass.EXAMPLE_SENTENCES, "foo"))
    }

    @Test
    fun testParse() {
        expect(DictionaryMeta.DictionaryID.EDICT_JB) { DictionaryMeta.DictionaryID.parse(DictionaryMeta.DictionaryID.EDICT_JB.toString()) }
        expect(DictionaryMeta.DictionaryID.JMNEDICT) { DictionaryMeta.DictionaryID.parse(DictionaryMeta.DictionaryID.JMNEDICT.toString()) }
        expect(DictionaryMeta.DictionaryID.KOTOWAZA) { DictionaryMeta.DictionaryID.parse(DictionaryMeta.DictionaryID.KOTOWAZA.toString()) }
    }
}
