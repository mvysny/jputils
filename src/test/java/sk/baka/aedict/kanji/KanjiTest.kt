/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import com.github.mvysny.dynatest.cloneBySerialization
import org.junit.jupiter.api.Test
import sk.baka.aedict.util.cloneByBoxing
import sk.baka.aedict.util.cloneByWrite
import kotlin.test.expect

/**
 * @author mvy
 */
class KanjiTest {
    @Test
    fun testCodePoint() {
        expect(0x72AD) { Kanji("犭").codePoint }
    }

    private fun getParts(kanji: Char): String = kanji.toKanji().parts.sorted().joinToString("")

    @Test
    fun testgetParts() {
        expect("一人八大目米貝頁") { getParts('類') }
        expect("丿乂口各囗夂攵艹艾足路") { getParts('蕗') }
        expect("一丨个丿亅人侖冂冊十小幺廾糸") { getParts('綸') }
        expect("一三丨二亠八六口囗女衣襄") { getParts('嬢') }
        expect("丿乂儿八土夂小幺攵糸") { getParts('綾') }
        expect("亠冖土宀心罒") { getParts('憲') }
        expect("一") { getParts('一') }
    }

    @Test
    fun testgetPartsSortedContainerFirst() {
        expect("大一人米頁貝八目") { Kanji("類").partsSortedContainerFirst }
        expect("艾艹路各夂攵乂丿足囗口") { Kanji("蕗").partsSortedContainerFirst }
        expect("侖个人冊一丨亅冂廾丿十糸小幺") { Kanji("綸").partsSortedContainerFirst }
        expect("六襄三二一丨八囗口衣亠女") { Kanji("嬢").partsSortedContainerFirst }
        expect("儿八夂攵乂丿土糸小幺") { Kanji("綾").partsSortedContainerFirst }
        expect("亠土宀冖心罒") { Kanji("憲").partsSortedContainerFirst }
        expect("") { Kanji("一").partsSortedContainerFirst }
    }

    @Test
    fun occurenceDoesNotSurviveSerialization() {
        expect(679) { '類'.toKanji().commonality }
        expect(679) { '類'.toKanji().cloneByBoxing().commonality }
        expect(679) { '類'.toKanji().cloneByWrite().commonality }
        expect(679) { '類'.toKanji().cloneBySerialization().commonality }
    }
}
