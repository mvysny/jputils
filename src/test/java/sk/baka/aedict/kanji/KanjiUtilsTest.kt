/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import com.github.mvysny.dynatest.DynaTest
import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.JLPTLevel
import sk.baka.aedict.util.codePoints
import kotlin.test.expect

class KanjiUtilsTest : DynaTest({

    test("Jlpt") {
        expect(JLPTLevel.N5) { Kanji('山').jlptLevel }
        expect(JLPTLevel.N4) { Kanji('週').jlptLevel }
        expect(JLPTLevel.N3) { Kanji('商').jlptLevel }
        expect(JLPTLevel.N1) { Kanji('司').jlptLevel }
        expect(null) { Kanji('可').jlptLevel }
        expect(JLPTLevel.N2) { Kanji('庁').jlptLevel }
    }

    test("split") {
        expect(listOf("しゅ", "じ", "ん")) { "しゅじん".splitKanaToDigraphs() }
        expect(listOf(" ", "しゅ", "a", "a")) { " しゅaa".splitKanaToDigraphs() }
        expect(listOf("しゅ", "う", "じ", "ん")) { "しゅうじん".splitKanaToDigraphs() }
    }

    test("ContainsKanjiOrKanaOnly") {
        expect(true) { "クヮルテット".containsKanaOrKanjiOnly() }
        expect(true) { "ゑゔぁんげりをん新劇場版".containsKanaOrKanjiOnly() }
        expect(false) { "bla".containsKanaOrKanjiOnly() }
        expect(true) { "しゅじん".containsKanaOrKanjiOnly() }
    }
})
