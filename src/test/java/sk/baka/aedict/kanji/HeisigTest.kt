/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import sk.baka.aedict.dict.Gloss
import sk.baka.aedict.util.Language
import sk.baka.aedict.util.cloneByBoxing
import kotlin.test.expect

class HeisigTest {
    private val h = Heisig(Gloss().add(Language.ENGLISH, "blabla"), 2.toShort(), 5.toByte(), 10)
    @Test
    fun testBoxable() {
        expect(h) { h.cloneByBoxing() }
    }
}
