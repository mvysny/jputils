/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.junit.jupiter.api.Test
import sk.baka.aedict.util.codePoints
import kotlin.test.expect

/**
 * @author mvy
 */
class PartsTest {
    /**
     * Returns parts for given kanji.
     * @param kanji the kanji to analyze
     * @return a part list, never null, may be empty if unknown kanji is supplied.
     */
    private fun getParts(kanji: Char) = Kanji(kanji).parts

    @Test
    fun testRadicals() {
        assertParts("八", getParts('八'))
        assertParts("大一人", getParts('大'))
        assertParts("大丶一人犬", getParts('犬'))
        assertParts("大冖丶一宀人犬", getParts('宊'))
        assertParts("人", getParts('人'))
        assertParts("个人", getParts('个'))
        assertParts("个一人今", getParts('今'))
        assertParts("入辶込", getParts('込'))
        assertParts("一亠耒未口囗木匚二丨丿八", getParts('耟'))
        assertParts("一亠耒未八亅丨木二丿丁", getParts('耓'))
        assertParts("氵十", getParts('汁'))
        assertParts("一氵口丁亅囗可", getParts('河'))
    }

    /**
     * Test for https://code.google.com/p/aedict/issues/detail?id=326
     */
    @Test
    fun testParts2() {
        assertParts("十刂千丿", getParts('刋'))
        assertParts("刂乂刈丿", getParts('刈'))
        assertParts("刂", getParts('刂'))
        assertParts("亠亡忄匸匚", getParts('忙'))
        assertParts("忄", getParts('忄'))
        assertParts("乙扌", getParts('扎'))
        assertParts("扌", getParts('扌'))
        assertParts("乙礻", getParts('礼'))
        assertParts("礻", getParts('礻'))
        assertParts("一亅疒丁", getParts('疔'))
        assertParts("疒", getParts('疒'))
        assertParts("乙卩犭㔾", getParts('犯'))
        assertParts("犭", getParts('犭'))
        assertParts("刀衤", getParts('初'))
        assertParts("衤", getParts('衤'))
        assertParts("木灬", getParts('杰'))
        assertParts("灬", getParts('灬'))
        assertParts("罒八貝目", getParts('買'))
        assertParts("罒", getParts('罒'))
        assertParts("丿虫囗厶丨冂中口禸禹", getParts('禹'))
        assertParts("冂厶禸", getParts('禸'))
        assertParts("丿虫囗厶冂口禸尸禹丨属中", getParts('嘱'))
    }

    @Test
    fun testParts3() {
        assertParts("一十昔日丨亻二廾丿", getParts('借'))
    }

    /**
     * https://code.google.com/p/aedict/issues/detail?id=356
     */
    @Test
    fun testParts4() {
        assertParts("時日寸寺土", getParts('時'))
        assertParts("寸寺牛土", getParts('特'))
        assertParts("一丨木亻本", getParts('体'))
        assertParts("十口氵囗舌丿活", getParts('活'))
        assertParts("一最耳日取又扌", getParts('撮'))
    }

    private fun assertParts(expected: String, got: Set<JpCharacter>) {
        val e = expected.codePoints.toSet()
        val g = got.joinToString("").codePoints.toSet()
        expect(e, "Expected $expected but got ${got.joinToString("")}") { g }
    }
}
