/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.inflection

import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.kanji.isKanji
import java.util.*

/**
 * @author mvy
 */
object AdjectiveInflection {
    /**
     * List of supported adjective forms.
     */
    @JvmStatic val FORMS: List<VerbInflection.Form> = listOf(
            VerbInflection.Form.PLAIN,
            VerbInflection.Form.POLITE,
            VerbInflection.Form.NEGATIVE,
            VerbInflection.Form.POLITE_NEGATIVE,
            VerbInflection.Form.PAST_TENSE,
            VerbInflection.Form.POLITE_PAST,
            VerbInflection.Form.NEGATIVE_PAST,
            VerbInflection.Form.POLITE_PAST_NEGATIVE)

    /**
     * Inflects an adjective.
     * @param base the adjective, not null. May be in hiragana or in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     * @param form one of [FORMS], not null.
     * @param type adj type such as Adj-i or Adj-na. Also supports いい and かっこいい (that is adj-ii/adj-yoi class).
     * @return inflected adjective forms, [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     * Usually this returns one item, but sometimes this returns two items, for example for "ku nai desu"/"ku arimasen".
     */
    fun inflect(base: String, form: VerbInflection.Form, type: AdjectiveType): List<String> {
        val romajiBase = IRomanization.NIHON_SHIKI.toRomaji(base)
        val adjix = type == AdjectiveType.AdjIX
        return if (type == AdjectiveType.AdjI || adjix) inflectADJI(romajiBase, form, adjix) else inflectADJNA(romajiBase, form)
    }

    /**
     * Inflects an adjective.
     * @param base the adjective, not null. must be in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     * @param form one of [FORMS], not null.
     * @return inflected adjective forms, [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     */
    private fun inflectADJI(base: String, form: VerbInflection.Form, adjix: Boolean): List<String> {
        @Suppress("NAME_SHADOWING")
        var base = base
        // allow inflecting of syunnei, which is adj-i according to JMDict.
        // https://github.com/mvysny/aedict/issues/595
        if (!base.endsWith("i")) {
            throw IllegalArgumentException("Parameter base: invalid value $base: not adj-i")
        }
        if (base == "ii") {
            base = "yoi"
        }
        if (adjix) {
            if (base.length >= 2 && base.dropLast(1).last().isKanji) {
                require(base.endsWith("i")) { "$base doesn't end with ii/yoi yet it's marked as ADJ-IX" }
            } else {
                require(base.endsWith("ii") || base.endsWith("yoi")) { "$base doesn't end with ii/yoi yet it's marked as ADJ-IX" }
            }
            if (base.endsWith("ii")) {
                base = base.substring(0, base.length - 2) + "yoi"
            }
        }
        base = base.substring(0, base.length - 1)
        return when (form) {
            VerbInflection.Form.PLAIN -> listOf(base + "i")
            VerbInflection.Form.POLITE -> listOf(base + "i desu")
            VerbInflection.Form.NEGATIVE -> listOf(base + "ku nai")
            VerbInflection.Form.POLITE_NEGATIVE -> listOf(base + "ku nai desu", base + "ku arimasen")
            VerbInflection.Form.PAST_TENSE -> listOf(base + "katta")
            VerbInflection.Form.POLITE_PAST -> listOf(base + "katta desu")
            VerbInflection.Form.NEGATIVE_PAST -> listOf(base + "ku nakatta")
            VerbInflection.Form.POLITE_PAST_NEGATIVE -> listOf(base + "ku nakatta desu", base + "ku arimasen desita")
            else -> throw IllegalArgumentException("Parameter form: invalid value $form: not applicable to adjectives")
        }
    }

    /**
     * Inflects an adjective.
     * @param base the adjective, not null. must be in [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki]
     * romanization.
     * @param form one of [FORMS], not null.
     * @return inflected adjective forms, [sk.baka.aedict.kanji.RomanizationEnum.NihonShiki] romanization.
     */
    private fun inflectADJNA(base: String, form: VerbInflection.Form): List<String> = when (form) {
        VerbInflection.Form.PLAIN -> listOf(base + " da")
        VerbInflection.Form.POLITE -> listOf(base + " desu")
        VerbInflection.Form.NEGATIVE -> listOf(base + " deha nai")
        VerbInflection.Form.POLITE_NEGATIVE -> listOf(base + " deha arimasen")
        VerbInflection.Form.PAST_TENSE -> listOf(base + " datta")
        VerbInflection.Form.POLITE_PAST -> listOf(base + " desita")
        VerbInflection.Form.NEGATIVE_PAST -> listOf(base + " deha nakatta")
        VerbInflection.Form.POLITE_PAST_NEGATIVE -> listOf(base + " deha arimasen desita")
        else -> throw IllegalArgumentException("Parameter form: invalid value $form: not applicable to adjectives")
    }
}

enum class AdjectiveType {
    AdjI,
    AdjIX,
    AdjNa
}

/**
 * I-adjective deinflector. I-adjectives all end in ~ i, although they never end in ~ ei (for example, kirei is not an i-adjective.)
 */
class IAdjectiveDeinflector : AbstractDeinflector {

    companion object {
        private val deinflectors: MutableList<AbstractDeinflector> = mutableListOf()
        init {
            fun d(ending: String, form: VerbInflection.Form) {
                deinflectors.add(EndsWithDeinflector(ending, true, form, mapOf("i" to InflectableWordClass.ADJ)))
                deinflectors.add(EndsWithDeinflector(ending.replace(" ", ""), true, form, mapOf("i" to InflectableWordClass.ADJ)))
            }

            d("ku arimasen desita", VerbInflection.Form.POLITE_PAST_NEGATIVE)
            d("ku nakatta desu", VerbInflection.Form.POLITE_PAST_NEGATIVE)
            d("ku nakatta", VerbInflection.Form.NEGATIVE_PAST)
            d("katta desu", VerbInflection.Form.POLITE_PAST)
            d("katta", VerbInflection.Form.PAST_TENSE)
            d("ku arimasen", VerbInflection.Form.POLITE_NEGATIVE)
            d("ku nai desu", VerbInflection.Form.POLITE_NEGATIVE)
            d("ku nai", VerbInflection.Form.NEGATIVE)
            d("i desu", VerbInflection.Form.POLITE)
        }
    }

    override var form: VerbInflection.Form? = null
        private set

    override fun deinflect(romaji: String): Map<String, EnumSet<InflectableWordClass>>? {
        require(romaji.isNotBlank()) { "romaji" }
        form = null
        for (deinflector: AbstractDeinflector in deinflectors) {
            var deinflected: String? = deinflector.deinflect(romaji)?.keys?.firstOrNull()
            if (deinflected != null) {
                require(deinflected.isNotBlank()) { "deinflected" }
                if (deinflected.endsWith("ei")) {
                    // i-adjectives never ends with ei
                    return null
                }
                if (deinflected == "yoi") {
                    deinflected = "ii"
                }
                form = deinflector.form
                val mapOf = mutableMapOf(deinflected to InflectableWordClass.ADJ)
                if (romaji.endsWith("katta") && romaji.length > 5) {
                    // 見tukatta -> 見tukaru ; musim pustit aj dalsie deinflectory
                    // 助katta
                    val c: Char = romaji.removeSuffix("katta").last()
                    if (c == 'u' || c.isKanji) {
                        mapOf[romaji] = InflectableWordClass.VERB_GODAN
                    }
                }
                return mapOf
            }
        }
        return null
    }

    override fun stopIfMatch(): Boolean {
        // matchnem sice tabenakatta ako mozne adjective, ale chcem to odsklonovat aj verb odsklonovacmi
        // takisto: would prevent 見tukatta from deinflecting to 見tukaru
        return false
    }
}
