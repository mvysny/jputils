/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.dict.Kanjidic2Entry.DicRef
import sk.baka.aedict.kanji.*
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box
import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.*

/**
 * Kanjidic entry, popisuje jeden kanji znak.
 * @param kanji Kanji, prave jeden codepoint.
 * @param strokes The stroke count of the kanji, including the radical.
 * @param skip SKIP, may be null (niektore kanji nemaju v KANJIDIC2 vyplneny skip).
 * @param grade The kanji grade level. 1 through 6 indicates a Kyouiku kanji
 * and the grade in which the kanji is taught in Japanese schools.
 * 8 indicates it is one of the remaining Jouyou Kanji to be learned
 * in junior high school, and 9 or 10 indicates it is a Jinmeiyou (for use
 * in names) kanji. G
 * @param onyomi the "on" Japanese reading of the kanji, in katakana. May be empty.
 * @param radicals A string containing all radicals, no separators, just the radical characters. Not null, may be empty.
 * Toto je skutocny radikal, nie vsetky casti (Kanji Parts). Max jeden znak.
 * @param kunyomi the "kun" Japanese reading of the kanji, usually in hiragana. Where relevant the okurigana is also included separated by a
 * ".". Readings associated with prefixes and suffixes are marked with a "-". May be empty. kunyomi je hiragana, ale moze byt aj katakana, napr. 吋 - インチ
 * @param meanings The meanings associated with the kanji. The key defines the target language of the meaning. It
 * will be coded using the two-letter language code from the ISO 639-1
 * standard. Immutable, never null, may be empty.
 * @param namae Citanie v menach. May be empty. Casto hiragana, ale pre kanji 丿 je T2 ノ, cize moze byt aj katakana.
 * @param pinyin the modern PinYin romanization of the Chinese reading
 * of the kanji. The tones are represented by a concluding
 * digit. Uses [sk.baka.aedict.kanji.PinyinRomanizationEnum.ToneNumerals] romanization, use [sk.baka.aedict.kanji.PinyinRomanizationEnum]
 * to convert.
 * @param heisig "Remembering The  Kanji"  by  James Heisig. Heisig Kanji index, napr. tu: http://ziggr.com/heisig/
 * @param dicRef Each dic_ref contains an index number. The particular dictionary,
 * etc. is defined by the dr_type attribute. Bud je tu Integer, alebo v pripade [DicRef.moro] tu moze byt Integer alebo [String] vo formate "2134 (vol 5 page 6)", alebo v pripade
 * [DicRef.busy_people] tu bude "vol 4 chapter 8". Null for older entries.
 * @param sh_desc the descriptor codes for The Kanji Dictionary (Tuttle 1996) by Spahn and Hadamitzky. They are in the form nxnn.n,
 * e.g.  3k11.2, where the  kanji has 3 strokes in the
 * identifying radical, it is radical "k" in the SH
 * classification system, there are 11 other strokes, and it is
 * the 2nd kanji in the 3k11 sequence. (I am very grateful to
 * Mark Spahn for providing the list of these descriptor codes
 * for the kanji in this file.) I
 * Fixes https://github.com/mvysny/aedict/issues/545
 * @param four_corner the "Four Corner" code for the kanji. This is a code
 * invented by Wang Chen in 1928. See the KANJIDIC documentation
 * for  an overview of  the Four Corner System. Q
 * @param deroo  the codes developed by the late Father Joseph De Roo, and
 * published in  his book "2001 Kanji" (Bonjinsha). Fr De Roo
 * gave his permission for these codes to be included. DR
 * @param freqNewspaper A frequency-of-use ranking. The 2,500 most-used characters have a
ranking; those characters that lack this field are not ranked. The
frequency is a number from 1 to 2,500 that expresses the relative
frequency of occurrence of a character in modern Japanese. This is
based on a survey in newspapers, so it is biassed towards kanji
used in newspaper articles. The discrimination between the less
frequently used kanji is not strong. (Actually there are 2,501
kanji ranked as there was a tie.). Fixes https://github.com/mvysny/aedict/issues/694
 * @param notToBeConfusedWith a set of kanjis with which this kanji is often confused with. See https://github.com/mvysny/aedict/issues/788 for details.
 */
data class Kanjidic2Entry(@JvmField val kanji: Kanji, @JvmField val strokes: Int, @JvmField val skip: SkipCode?, @JvmField val grade: Int?,
                          @JvmField val onyomi: List<String>, @JvmField val kunyomi: List<String>, @JvmField val meanings: Gloss,
                          @JvmField val radicals: String, @JvmField val namae: List<String>,
                          @JvmField val pinyin: List<String>, @JvmField val heisig: Heisig?,
                          @JvmField val dicRef: EnumMap<Kanjidic2Entry.DicRef, Any>?,
                          @JvmField val sh_desc: String?,
                          @JvmField val four_corner: String?, @JvmField val deroo: String?, @JvmField val freqNewspaper: Int?,
                          @JvmField val notToBeConfusedWith: Set<Kanji>
                          ) : Serializable, Writable, IDisplayableEntry, Boxable {

    fun parsePinyin(): List<Pinyin> = pinyin.map { Pinyin.parse(it) }

    init {
        require (strokes >= 1) { "Parameter strokes: invalid value $strokes: must be 1 or greater" }
        require (grade == null || grade in 1..10) { "Parameter grade: invalid value $grade: must be 1..10" }
        require (freqNewspaper == null || freqNewspaper in 1..2501) { "Parameter freqNewspaper: invalid value $freqNewspaper: must be 1..2501" }
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(CURRENT_VERSION.toInt())
        // v3
        box().writeTo(out)
        // v2
        //        kanji.writeTo(out);
        //        out.writeByte(strokes);
        //        out.writeBoolean(skip != null);
        //        if (skip != null) {
        //            skip.writeTo(out);
        //        }
        //        out.writeUTF(radicals);
        //        out.writeByte(grade == null ? 0 : grade);
        //        Writables.writeListOfStrings(onyomi, out);
        //        Writables.writeListOfStrings(kunyomi, out);
        //        meanings.writeTo(out);
        //        Writables.writeListOfStrings(namae, out);
        //        Writables.writeListOfStrings(pinyin, out);
        //        out.writeInt(heisig == null ? -1 : heisig);
    }

    override fun box(): Box {
        return Box().putBoxable("kanji", kanji)
                .putByte("strokes", strokes.toByte())
                .putBoxable("skip", skip)
                .putString("radical", radicals)
                .putByte("grade", grade?.toByte())
                .putListOfBasics("onyomi", onyomi)
                .putListOfBasics("kunyomi", kunyomi)
                .putBoxable("meanings", meanings)
                .putListOfBasics("namae", namae)
                .putListOfBasics("pinyin", pinyin)
                .putBoxable("heisig", heisig)
                .putMapOfBasics("dicRef", dicRef)
                .putString("sh_desc", sh_desc)
                .putString("four_corner", four_corner)
                .putString("deroo", deroo)
                .putString("notToBeConfusedWith", notToBeConfusedWith.joinToString(""))
        .putInt("freqNewspaper", freqNewspaper)
    }

    override fun getJapaneseReading(romanization: IRomanization): String {
        val readings = onyomi + kunyomi
        val result = StringBuilder()
        readings.joinTo(result)
        if (!namae.isEmpty()) {
            result.append(" [")
            namae.joinTo(result)
            result.append(']')
        }
        return romanization.toRomaji(result.toString())
    }

    override fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String =
        meanings.getAndPreferLang(preferLang).joinToString()

    override fun getRubyFuriganas(): List<RubyFurigana> = readings.map { RubyFurigana(kanji.kanji(), it) }

    override fun getSenses() = meanings

    /**
     * Returns JLPT level of given kanji.
     * @return JLPT level or null if the kanji is not present in any of the JLPT test.
     */
    val jlptLevel: JLPTLevel?
        get() = kanji.jlptLevel

    /**
     * Pozor nepreusporaduvat konstanty, nevymazavat konstanty! Idu do Box.putEnumMapOfBasics()
     */
    enum class DicRef(@JvmField val title: String, @JvmField val description: String) {
        nelson_c("Nelson Classic", "\"Modern Reader's Japanese-English Character Dictionary\", edited by Andrew Nelson (now published as the \"Classic\" Nelson)."),
        nelson_n("New Nelson", "\"The New Nelson Japanese-English Character Dictionary\", edited by John Haig."),
        halpern_njecd("Halpern New Dict", "\"New Japanese-English Character Dictionary\", edited by Jack Halpern."),
        halpern_kkld("Kodansha", "\"Kanji Learners Dictionary\" (Kodansha) edited by Jack Halpern."),
        gakken("Gakken", "\"A  New Dictionary of Kanji Usage\" (Gakken)"),
        oneill_names("O'Neill Names", "\"Japanese Names\", by P.G. O'Neill. "),
        oneill_kk("O'Neill Essential", "\"Essential Kanji\" by P.G. O'Neill."),
        moro("Morohashi", "\"Daikanwajiten\" compiled by Morohashi"),
        henshall("Henshall", "\"A Guide To Remembering Japanese Characters\" by Kenneth G.  Henshall."),
        sh_kk("Kanji and Kana", "\"Kanji and Kana\" by Spahn and Hadamitzky."),
        sakade("Sakade", "\"A Guide To Reading and Writing Japanese\" edited by Florence Sakade."),
        jf_cards("Hodges-Okazaki", "Japanese Kanji Flashcards, by Max Hodges and Tomoko Okazaki. (Series 1)"),
        henshall3("Henshall 3rd ed", "\"A Guide To Reading and Writing Japanese\" 3rd edition, edited by Henshall, Seeley and De Groot."),
        tutt_cards("Tuttle Kanji", "Tuttle Kanji Cards, compiled by Alexander Kask."),
        crowley("Crowley", "\"The Kanji Way to Japanese Language Power\" by Dale Crowley."),
        kanji_in_context("Kanji in Context", "\"Kanji in Context\" by Nishiguchi and Kono."),
        busy_people("Busy People", "\"Japanese For Busy People\" vols I-III, published by the AJLT. The codes are the volume.chapter."),
        kodansha_compact("Kodansha Compact", "the \"Kodansha Compact Kanji Guide\"."),
        maniette("Maniette", "codes from Yves Maniette's \"Les Kanjis dans la tete\" French adaptation of Heisig."),
        halpern_kkd("Kodansha 2nd ed NJECD", "\"Kodansha Kanji Dictionary\", (2nd Ed. of the NJECD) edited by Jack Halpern."),
        halpern_kkld_2ed("Kodansha 2nd ed", "\"Kanji Learners Dictionary\" (Kodansha), 2nd edition (2013) edited by Jack Halpern."),
        sh_kk2("Kanji and Kana 2011", "\"Kanji and Kana\" by Spahn and Hadamitzky (2011 edition).")
    }

    override val kanjis: LinkedHashSet<String> get() = LinkedHashSet(setOf(kanji.kanji()))

    override val readings: LinkedHashSet<String> get() {
        val result = LinkedHashSet<String>()
        result.addAll(onyomi)
        result.addAll(kunyomi)
        result.addAll(namae)
        return result
    }

    companion object : Unboxable<Kanjidic2Entry>, Readable<Kanjidic2Entry> {

        // s Box netreba uz dvihat verziu
        private val CURRENT_VERSION: Byte = 3

        @JvmStatic
        override fun readFrom(di: DataInput): Kanjidic2Entry {
            val ver = di.readByte()
            // s box sme dopredne kompatibilni
            //        if (ver > CURRENT_VERSION) {
            //            throw new RuntimeException("Unreadable version: got " + ver + " but support only up to " + CURRENT_VERSION);
            //        }
            if (ver >= 3) {
                return unbox(Box.readFrom(di))
            }
            val kanji = Kanji.readFrom(di)
            val strokes = di.readByte().toInt()
            val skipNotNull = di.readBoolean()
            val skip = if (skipNotNull) SkipCode.readFrom(di) else null
            val radicals = di.readUTF()
            var grade: Int? = di.readByte().toInt()
            if (grade == 0) {
                grade = null
            }
            val onyomi = Writables.readListOfStrings(di)!!
            val kunyomi = Writables.readListOfStrings(di)!!
            val meanings = Gloss.readFrom(di)
            val namae = Writables.readListOfStrings(di)!!
            val pinyin = (if (ver < 1) emptyList<String>() else Writables.readListOfStrings(di))!!
            if (ver >= 2) di.readInt() // heisig
            return Kanjidic2Entry(kanji, strokes, skip, grade, onyomi, kunyomi, meanings, radicals, namae, pinyin, null,
                    null, null, null, null, null, setOf())
        }

        @JvmStatic
        override fun unbox(box: Box): Kanjidic2Entry {
            val grade = box.get("grade").byteValue().orNull()
            val dicRef = box.get("dicRef").enumMapOfBasics<DicRef, Any>(DicRef::class.java).orNull()
            val notToBeConfusedWith = box.get("notToBeConfusedWith").string().or("").getUniqueKanjis()
            return Kanjidic2Entry(
                    box.get("kanji").boxable(Kanji::class.java).get(),
                    box.get("strokes").byteValue().get().toInt(),
                    box.get("skip").boxable(SkipCode::class.java).orNull(),
                    grade?.toInt(),
                    box.get("onyomi").listOf(String::class.java).get(),
                    box.get("kunyomi").listOf(String::class.java).get(),
                    box.get("meanings").boxable(Gloss::class.java).get(),
                    box.get("radical").string().get(),
                    box.get("namae").listOf(String::class.java).get(),
                    box.get("pinyin").listOf(String::class.java).get(),
                    box.get("heisig").boxable(Heisig::class.java).orNull(),
                    dicRef,
                    box.get("sh_desc").string().orNull(),
                    box.get("four_corner").string().orNull(),
                    box.get("deroo").string().orNull(),
                    box.get("freqNewspaper").integer().orNull(),
                    notToBeConfusedWith)
        }

        @JvmStatic
        fun mock(kanji: Kanji, strokes: Int = 1, sense: String = "Not a regular Japanese kanji"): Kanjidic2Entry =
                Kanjidic2Entry(kanji, strokes, null, null, listOf(),
                        listOf(),
                        Gloss().add(Language.ENGLISH, sense),
                        "",
                        listOf(),
                        listOf(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        setOf())
    }

    val isMocked: Boolean
        get() = onyomi.isEmpty() && kunyomi.isEmpty() && namae.isEmpty() && radicals == "" && pinyin.isEmpty()
}
