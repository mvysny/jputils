/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.util.typedmap.Box
import sk.baka.aedict.util.Boxable
import sk.baka.aedict.util.Readable
import sk.baka.aedict.util.Unboxable
import sk.baka.aedict.util.Writable

import java.io.*
import java.util.NoSuchElementException
import java.util.Scanner

/**
 * The SKIP code.
 */
data class SkipCode(
        /**
         * The visual style of the kanji, 1..4. The first number in the SKIP code.
         *  1. style 1 : into left and right sides, such as:
         *  2. style 2 : into top and bottom parts, such as:
         *  3. style 3 : into an enclosing structure and an internal structure, such as:       ; some of these can be a bit tricky.
         *  4. style 4 : none of the above, such as:
         */
        val visualStyle: Int,
        /**
         * For styles 1, 2 and 3 count the strokes in the left/upper/enclosing part.
         * These two numbers, along with the 1, 2 or 3, form your SKIP code.
         *
         * If the SKIP code is of style 4, count all the strokes in the character.
         */
        val strokes1: Int,
        /**
         * For styles 1,2 and 3 count then the number of strokes left in the right/lower/internal part.
         *
         * If [visualStyle] is 4, choose the first from (inserting the stroke-count where the ? is shown):
         *  1. 1 : has a horizontal line at the top (e.g.   4-3-1,  4-6-1).
         *  2. 2 : has a horizontal line at the bottom (e.g.  4-3-2,  4-6-2).
         *  3. 4-?-3 : has a vertical line through (e.g.  4-4-3,  4-8-3).
         *  4. 4-?-4 : none (e.g.  4-4-4,  4-3-4).
         *
         */
        val strokes2: Int) : Serializable, Writable, Boxable {
    /**
     * Returns the number of strokes in the kanji.
     * @return number of strokes.
     */
    val strokeCount: Int
        get() = if (visualStyle == 4) strokes1 else strokes1 + strokes2

    init {
        require (visualStyle in 1..4) { "Parameter visualStyle: invalid value $visualStyle: must be 1..4: $this" }
        if (visualStyle == 4) {
            require (strokes2 in 1..4) { "Parameter strokes2: invalid value $strokes2: must be 1..4: $this" }
        }
        require (strokes1 >= 1) { "Parameter strokes1: invalid value $strokes1: must be 1 or greater: $this" }
        require (strokes2 >= 1) { "Parameter strokes2: invalid value $strokes2: must be 1 or greater: $this" }
    }

    override fun toString() = "$visualStyle-$strokes1-$strokes2"

    /**
     * Returns the SKIP code in the form of x-y-z.
     * @return the SKIP code, never null.
     */
    fun toSkip() = toString()

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(visualStyle)
        out.writeByte(strokes1)
        out.writeByte(strokes2)
    }

    override fun box(): Box {
        return Box()
                .putByte("visualStyle", visualStyle.toByte())
                .putByte("strokes1", strokes1.toByte())
                .putByte("strokes2", strokes2.toByte())
    }

    companion object : Unboxable<SkipCode>, Readable<SkipCode> {

        /**
         * Parses the SKIP code in the form of x-y-z
         * @param skip the SKIP code, not null.
         * @return parsed skip code, not null.
         * @throws IllegalArgumentException if the skip cannot be parsed.
         */
        @JvmStatic
        fun parse(skip: String): SkipCode {
            try {
                val s = Scanner(skip).useDelimiter("-")
                val visualStyle = s.nextInt()
                val strokes1 = s.nextInt()
                val strokes2 = s.nextInt()
                if (s.hasNext()) {
                    throw IllegalArgumentException("$skip: Expected only three numbers in the form of x-y-z")
                }
                return SkipCode(visualStyle, strokes1, strokes2)
            } catch (ex: NoSuchElementException) {
                throw IllegalArgumentException("$skip: Expected three numbers in the form of x-y-z", ex)
            }
        }

        @JvmStatic
        @Throws(IOException::class)
        override fun readFrom(di: DataInput): SkipCode {
            val visualStyle = di.readByte()
            val strokes1 = di.readByte()
            val strokes2 = di.readByte()
            return SkipCode(visualStyle.toInt(), strokes1.toInt(), strokes2.toInt())
        }

        @JvmStatic
        override fun unbox(box: Box) = SkipCode(
                    box.get("visualStyle").byteValue().get().toInt(),
                    box.get("strokes1").byteValue().get().toInt(),
                    box.get("strokes2").byteValue().get().toInt())
    }
}
