/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.dict

import sk.baka.aedict.kanji.IRomanization
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.DataInput
import java.io.DataOutput
import java.io.IOException
import java.io.Serializable
import java.util.LinkedHashSet

/**
 * An example sentences, taken from Tanaka or Tatoeba.
 * @property kanji The original japanese example sentence, containing all kanjis. Never blank.
 * @property reading The reading of [kanji], katakana or hiragana without any kanji. Never blank.
 * @property words The b-line of the example sentence. Empty if not known.
 * @property furiganaViewFormInt The Japanese sentence with the furigana characters (katakana or hiragana), in the form of {kanji;reading}.
 * May be null for old entries. If not null, will not be blank.
 */
data class ExamplesEntry(@JvmField val sentences: Gloss, @JvmField val kanji: String, @JvmField val reading: String,
                    @JvmField val words: List<Word>, @JvmField val tatoebaJPId: Int?,
                    private val furiganaViewFormInt: String?) : Writable, Serializable, IDisplayableEntry, Boxable {

    /**
     * A word in the example sentence. Contains B-indices from [Tanaka corpus](http://www.edrdg.org/wiki/index.php/Tanaka_Corpus#Current_Format_.28WWWJDIC.29).
     * There is a [documentation on the B-indices format](http://www.edrdg.org/wiki/index.php/Sentence-Dictionary_Linking):
     *
     * The indices for a sentence consist of a line of text with space-delimited index elements for each word in the sentence. The following is an example:
     *
     * Sentence: その家はかなりぼろ屋になっている。
     *
     * Indices: `其の[01]{その} 家(いえ)[01] は 可也{かなり} ぼろ屋[01]~ になる[01]{になっている}`
     *
     * The format of the index elements is as follows:
     * * the usual headword as it appears in the dictionary. Even if the word is usually
     *   written in kana, the kanji form must be used if it is available. This field is mandatory,
     *   however it may be omitted for proper names not found in the dictionary.
     * * the reading of the word in kana, or the numerical sequence number of the appropriate
     *   entry in the JMdict dictionary in the format "#nnnnnnnn". This is optional, however
     *   it must be used if there are several different dictionary entries with the same headword.
     *   This field is in regular parentheses.
     * * a sense number. This is used when the word has multiple senses in the JMdict/EDICT2 file,
     *   and indicates which sense applies in the sentence. It is a two-digit numeric in square parentheses. The field is optional.
     * * the form in which the word appears in the sentence. This may differ from the indexing word,
     *   e.g. if it is an inflected verb or adjective, if the word is usually written in a different way, etc.
     *   This field is in "curly" parentheses. It is not mandatory, but should be included where appropriate.
     * * a "~" character to indicate that the sentence pair is a good and checked example of the usage of the word.
     *   Words are marked to enable appropriate sentences to be selected by dictionary software.
     *   Typically only one instance per sense of a word will be marked. (The WWWJDIC server displays
     *   these sentences below the display of the related dictionary entry.)
     *
     * Some indices are followed by a "|" character and a digit. These are an artefact from a former maintenance system, and can be safely ignored.
     *
     * The fields after the indexing headword ()[]{}~ must be in that order.
     *
     * Examples:
     * * [920](https://github.com/mvysny/aedict/issues/920): sentence アレックスは、「クルミ」や、「紙」や、「とうもろこし」のような物の名前を言うことができる
     *    * for bword: は|1 je dictionaryForm=は, reading=は, inSentence=は
     *    * for bword: 様(よう){ような} je dictionaryForm=様, reading=よう, inSentence=ような
     *    * for bword: 物(もの) je dictionaryForm=物, reading=もの, inSentence=物
     * * Another example: bword 十分(じっぷん){10分}: dictionaryForm=十分, reading=じっぷん, inSentence=10分
     * * bword 人々: dictionaryForm=reading=inSentence=人々
     *
     * @property dictionaryForm the dictionary form of the word. May contain only hiragana, katakana and/or kanji. Not blank.
     * Does not contain the "|" character.
     * @property reading reading of the dictionary form; if the reading is not known then this is equal to [dictionaryForm].
     * @property inSentence Returns the word as it appeared in the sentence. Should contain only hiragana, katakana and/or kanji,
     * but it might contain other stuff, e.g. `10分`.
     * null if not known (in case of older dictionary data). May be equal to [dictionaryForm] or [reading] if
     * the word appears in the sentence in its [dictionaryForm] or [reading].
     */
    data class Word(val dictionaryForm: String, val reading: String,
                    val inSentence: String?) : Serializable {
        init {
            require(dictionaryForm.isNotBlank()) { "dictionaryForm is blank" }
            require(!dictionaryForm.contains('|')) { "$dictionaryForm contains |" }
            require(reading.isNotBlank()) { "reading is blank" }
            if (inSentence != null) {
                require(inSentence.isNotBlank()) { "inSentence is blank" }
            }
        }

        override fun toString(): String = "$dictionaryForm($reading){$inSentence}"
    }

    /**
     * If false, this entry is old and [furiganaViewForm] will just equal to [reading].
     */
    val hasFurigana get() = furiganaViewFormInt != null

    /**
     * The Japanese sentence with the furigana characters, in the form of {kanji;reading}. Never blank. May be equal to [reading] for old entries.
     */
    val furiganaViewForm: String get()= furiganaViewFormInt ?: reading

    /**
     * Returns the http://tatoeba.org/... url for online tatoeba sentence.
     * @return tatoeba site URL or null if [tatoebaJPId] is null.
     */
    val tatoebaSentenceURL: String?
        get() {
            if (tatoebaJPId == null) {
                return null
            } else if (tatoebaJPId == KOTOWAZA_PROVERBS) {
                return "http://www.kotowaza.org/"
            } else {
                return "http://tatoeba.org/eng/sentences/show/$tatoebaJPId"
            }
        }

    init {
        require (kanji.isNotBlank()) { "kanji" }
        require (reading.isNotBlank()) { "reading" }
    }

    @Throws(IOException::class)
    override fun writeTo(out: DataOutput) {
        out.writeByte(VERSION.toInt())
        // v2
        box().writeTo(out)
        // v1
        //        sentences.writeTo(out);
        //        out.writeUTF(kanji);
        //        out.writeUTF(reading);
        //        Writables.writeListOfStrings(deinflectedWords, out);
        //        out.writeInt(tatoebaJPId == null ? -1 : tatoebaJPId);
    }

    override fun box(): Box {
        return Box()
                .putBoxable("sentences", sentences)
                .putString("kanji", kanji)
                .putString("reading", reading)
                .putListOfBasics("deinflectedWords", words.map { it.dictionaryForm })
                .putListOfBasics("deinflectedWordsReading", words.map { it.reading })
                .putListOfBasics("wordsInSentence", words.map { it.inSentence })
                .putInt("tatoebaJPId", tatoebaJPId)
                .putString("furiganaViewForm", furiganaViewFormInt)
    }

    override fun toString() = "{$kanji/$reading: $sentences}"

    override fun getJapaneseReading(romanization: IRomanization): String = romanization.toRomaji(reading)

    override fun getSenseOneLiner(preferLang: Language?, includeFlags: Boolean): String =
            sentences.getAndPreferLang(preferLang).joinToString()

    override fun getRubyFuriganas(): List<RubyFurigana> = listOf(RubyFurigana(kanji, reading))

    override val kanjis: LinkedHashSet<String> get() = LinkedHashSet(setOf(kanji))

    override val readings: LinkedHashSet<String> get() = LinkedHashSet(setOf(reading))

    override fun getSenses(): Gloss = sentences

    companion object : Unboxable<ExamplesEntry>, Readable<ExamplesEntry> {
        // mame Box, uz netreba dvihat verziu
        private val VERSION: Byte = 2

        @JvmStatic
        val KOTOWAZA_PROVERBS = -20

        @Throws(IOException::class)
        @JvmStatic
        override fun readFrom(di: DataInput): ExamplesEntry {
            val version = di.readByte()
            // no need - Box is forward-compatible
            //        if (version > VERSION) {
            //            throw new RuntimeException("Incompatible version: " + version + ", at most " + VERSION + " is supported");
            //        }
            if (version >= 2) {
                return ExamplesEntry.unbox(Box.readFrom(di))
            }
            val gloss = Gloss.readFrom(di)
            val kanji = di.readUTF()
            val reading = di.readUTF()
            val deinflectedWords = Writables.readListOfStrings(di)!!
// nikdy nepiseme null
            var tatoebaJPId: Int? = null
            if (version >= 1) {
                tatoebaJPId = di.readInt()
                if (tatoebaJPId < 0) {
                    tatoebaJPId = null
                }
            }
            val words = deinflectedWords.map {
                val w = it.substringBefore('|')
                Word(w, w, null)
            }
            return ExamplesEntry(gloss, kanji, reading, words, tatoebaJPId, null)
        }

        @JvmStatic
        override fun unbox(box: Box): ExamplesEntry {
            var deinflectedWords = box.get("deinflectedWords").listOf(String::class.java).get()
            // remove pipe and anything after the pipe. Explanation:
            // Some indices are followed by a "|" character and a digit. These are an artefact from a former maintenance system, and can be safely ignored.
            deinflectedWords = deinflectedWords.map { it.substringBefore('|') }
            var deinflectedWordsReading = box.get("deinflectedWordsReading").listOf(String::class.java).or(deinflectedWords)
            deinflectedWordsReading = deinflectedWordsReading.map { it.substringBefore('|') }
            val wordsInSentence: List<String?> = box.get("wordsInSentence").listOf(String::class.java).or(List(deinflectedWords.size) { null })
            val words: List<Word> = deinflectedWords.indices.map {
                Word(deinflectedWords[it], deinflectedWordsReading[it], wordsInSentence[it])
            }
            return ExamplesEntry(
                    box.get("sentences").boxable(Gloss::class.java).get(),
                    box.get("kanji").string().get(),
                    box.get("reading").string().get(),
                    words,
                    box.get("tatoebaJPId").integer().orNull(),
                    box.get("furiganaViewForm").string().orNull())
        }
    }
}
