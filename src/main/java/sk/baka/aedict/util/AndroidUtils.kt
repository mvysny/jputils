/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import android.content.SharedPreferences
import android.os.Looper
import net.iharder.Base64
import sk.baka.aedict.util.typedmap.internalJava
import java.io.ByteArrayInputStream
import java.lang.reflect.Modifier
import java.util.zip.GZIPInputStream
import kotlin.reflect.KProperty

val isUIThread: Boolean
get() = Looper.myLooper() != null

fun checkUIThread() {
    if (!isUIThread) throw IllegalStateException("Invalid state: not invoked in the UI thread");
}

fun checkNotUIThread() {
    if (isUIThread) throw IllegalStateException("Invalid state: invoked in the UI thread");
}

/**
 * This allows for value classes backed by Android [SharedPreferences] class. The properties may be specified simply as follows:
 * `var recentEntriesCount: Int by prefs.default(15)`
 */
data class KSharedPreferences(val prefs: SharedPreferences, val key: String? = null, val default: Any? = null,
                              val validator: (Any?)->Unit = {}) {
    fun default(default: Any) = copy(default = default)
    fun key(key: String) = copy(key = key)
    fun validator(validator: (Any?)->Unit) = copy(validator = validator)
    @Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")
    operator inline fun <reified R : Any?> getValue(thisRef: Any?, property: KProperty<*>): R {
        val key: String = this.key ?: property.name
        val v = synchronized(prefs) { prefs.all[key] ?: default }
        if (v == null) return null as R
        val clazz = R::class.internalJava
        if (clazz.isEnum) {
            return when (v) {
                is String -> Enums.enumValueOf(clazz, v) as R
                is Number -> clazz.enumConstants[v.toInt()] as R
                else -> v as R
            }
        }
        when (R::class) {
            Integer::class -> return if (v is String) v.toInt() as R else v as R
            java.lang.Float::class -> return v as R
            String::class -> return v as R
            java.lang.Short::class -> return if (v is String) v.toShort() as R else (v as Int).toShort() as R
            java.lang.Byte::class -> return if (v is String) v.toByte() as R else (v as Int).toByte() as R
            java.lang.Boolean::class -> return v as R
            java.lang.Long::class -> return if (v is String) v.toLong() as R else v as R
            IntArray::class -> return toIntArray(v as String) as R
            BooleanArray::class -> return toBooleanArray(v as String) as R
        }
        if (Boxable::class.java.isAssignableFrom(R::class.java)) {
            require(R::class.java.isFinal) { "jputils/AndroidUtils.kt/KSharedPreferences.getValue(): ${R::class.java} is not final" }
            val bytes = (v as String).toByteArray(Charsets.US_ASCII)
            return GZIPInputStream(Base64.InputStream(ByteArrayInputStream(bytes))).use { input ->
                @Suppress("UNCHECKED_CAST")
                Boxables.read(R::class.java as Class<Boxable>, input) as R
            }
        }
        throw RuntimeException("jputils/AndroidUtils.kt/KSharedPreferences.getValue(): Unsupported type ${R::class}")
    }
    fun toIntArray(str: String?): IntArray? {
        str ?: return null
        return str.split('|').filterNotBlank().map { it.toInt() }.toIntArray()
    }
    fun toBooleanArray(str: String?): BooleanArray? {
        val a = toIntArray(str) ?: return null
        val result = BooleanArray(a[0])
        for (i in 1..a.size - 1) {
            result[a[i]] = true
        }
        return result
    }
    fun booleanArrayToString(value: BooleanArray): String {
        val trues = value.count { it }
        val a = IntArray(trues + 1)
        a[0] = value.size
        var j = 1
        for (i in 0..value.size - 1) {
            if (value[i]) {
                a[j++] = i
            }
        }
        return a.joinToString("|")
    }
    operator inline fun <reified R : Any?> setValue(thisRef: Any?, property: KProperty<*>, value: R) {
        validator(value)
        val key: String = this.key ?: property.name
        synchronized(prefs) {
            prefs.edit().apply {
                if (R::class.internalJava.isEnum) {
                    putString(key, (value as Enum<*>).name)
                } else when (value) {
                    // vkladaj cisla ako Stringy, inac jebnuty Android failuje s exception
                    // https://github.com/mvysny/aedict/issues/711
                    null -> remove(key)
                    is Boolean -> putBoolean(key, value)
                    is Float -> putFloat(key, value)
                    is String -> putString(key, value)
                    is Int -> putString(key, value.toString())
                    is Long -> putString(key, value.toString())
                    is Short -> putString(key, value.toString())
                    is Byte -> putString(key, value.toString())
                    is IntArray -> putString(key, value.joinToString("|"))
                    is BooleanArray -> putString(key, booleanArrayToString(value))
                    is Boxable -> {
                        require(R::class.java.isFinal) { "jputils/AndroidUtils.kt/KSharedPreferences.setValue(): ${R::class.java} is not final" }
                        putString(key, value.boxToString(true))
                    }
                    else -> throw IllegalArgumentException("jputils/AndroidUtils.kt/KSharedPreferences.setValue(): Key $key: unsupported value of type ${(value as Any).javaClass}: $value")
                }
                apply()
            }
        }
    }
}

fun SharedPreferences.default(default: Any) = KSharedPreferences(this, default = default)
fun SharedPreferences.key(key: String) = KSharedPreferences(this, key = key)
fun SharedPreferences.validator(validator: (Any?)->Unit) = KSharedPreferences(this, validator = validator)
val SharedPreferences.kotlin: KSharedPreferences
        get() = KSharedPreferences(this)

val Class<*>.isFinal: Boolean get() = Modifier.isFinal(modifiers)
