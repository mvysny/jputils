/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import net.iharder.Base64
import sk.baka.aedict.util.typedmap.Box
import java.io.*
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

/**
 * A stable serialization. Serializable/Parcelable format may change, this must not.
 *
 * Anyone implementing this interface must have a static method `unbox(Box box)`
 *
 * Backward compatibility tips:
 * * Don't change type of the value stored under a key - it's better to create a new key,
 * check existing value upon unboxing, convert the value to the new key and delete the old key.
 * Otherwise you'll get a ClassCastException.
 * @author mvy
 */
interface Boxable {
    /**
     * Packs the boxable into the box.
     * @return the box, not null.
     */
    fun box(): Box
}

/**
 * Writes this writable directly to a file. A temp file is first created; after the write is successful,
 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
 * @param writable the writable, not null.
 * @param target the file to write to, not null
 * @param gzip if true, the stream is gzipped.
 */
fun Boxable.boxToFile(target: File, gzip: Boolean = false) = if (gzip) Boxables.writeGzipped(this, target) else Boxables.write(this, target)

/**
 * Writes this writable to given output stream.
 * @param stream output stream, NOT CLOSED, flushed. Automatically wrapped in {@link BufferedOutputStream}
 */
fun Boxable.boxToStream(stream: OutputStream): Unit = box().writeToStream(stream)

/**
 * Writes this writable to a byte array and returns that very byte array.
 */
fun Boxable.boxToBytes() = ByteArrayOutputStream().use { boxToStream(it); it }.toByteArray()

/**
 * Boxes this box into a base64-encoded string, optionally gzipped. Use [Unboxable.unboxFromString] to unbox it.
 * @return base64 string
 */
fun Boxable.boxToString(gzip: Boolean = false): String {
    val out = ByteArrayOutputStream()
    var stream: OutputStream = Base64.OutputStream(out)
    if (gzip) stream = GZIPOutputStream(stream)
    stream.use { boxToStream(it) }
    return out.toString(Charsets.US_ASCII.name())
}

/**
 * Implemented by [Boxable]'s companion object.  Do not forget to annotate [Unboxable.unbox] with [JvmStatic] so that it is backward-compatible!
 */
interface Unboxable<out T: Boxable> {
    /**
     * Unpacks the object back.
     * @param box the packed box
     * @return the unboxed object instance, never null.
     */
    fun unbox(box: Box): T

    /**
     * Unpacks the object from given stream. The stream is always closed.
     * @param stream the stream to read from, always closed.
     * @return the unboxed object instance, never null.
     */
    fun unboxFromStream(stream: InputStream): T = stream.use { unbox(Box.readFrom(DataInputStream(it.buffered()))) }

    /**
     * Reads the box contents from given file.
     * @param file the file with the box, not null.
     * @param gunzip if true, apply gunzip.
     * @return the unboxed object instance.
     */
    fun unboxFromFile(file: File, gunzip: Boolean = false): T {
        var stream: InputStream = file.inputStream()
        if (gunzip) stream = GZIPInputStream(stream)
        return unboxFromStream(stream)
    }

    /**
     * Reads the box contents from given byte array.
     * @param file the file with the box, not null.
     * @return the unboxed object instance.
     */
    fun unboxFromBytes(bytes: ByteArray): T = unboxFromStream(bytes.inputStream())

    /**
     * Unboxes this object from a string produced by [Boxable.boxToString].
     * @param string the base64-encoded bytes
     * @param gunzip if the stream was gzipped, set true here.
     * @return the unboxed object instance.
     */
    fun unboxFromString(string: String, gunzip: Boolean = false): T {
        var input: InputStream = Base64.InputStream(string.toByteArray(Charsets.US_ASCII).inputStream())
        if (gunzip) input = GZIPInputStream(input)
        return unboxFromStream(input)
    }
}

inline fun <reified T: Boxable> T.cloneByBoxing(): T = Boxables.read(T::class.java, Writables.clone(box())!!)
