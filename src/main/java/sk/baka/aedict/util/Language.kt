/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import java.io.Serializable
import java.util.HashMap
import java.util.Locale

/**
 * Denotes a language. See http://en.wikipedia.org/wiki/List_of_ISO_639-1_codes for a list of languages.
 * @param lang 2-letter ISO 639-1 or 3-letter ISO 639-2/T or 3-letter ISO 639-2/B language code, not null.
 */
class Language(lang: String) : Serializable, Comparable<Language> {
    /**
     * 3-letter lower-case ISO 639-2/T language code, never null. Always contains 3 characters.
     */
    @JvmField val lang3: String

    /**
     * Returns the 3-letter lower-case ISO 639-2/B language code. Always contains 3 characters.
     * @return the 3-letter lower-case ISO 639-2/B language code, never null
     */
    val ISO639BCode: String
        get() {
            val result = ISO_T_B[lang3]
            return result ?: lang3
        }

    init {
        if (lang.length < 2 || lang.length > 3) {
            throw IllegalArgumentException("Parameter lang: invalid value $lang: expected 2- or 3- letter ISO 639 code")
        }
        if (lang != lang.toLowerCase()) {
            throw IllegalArgumentException("Parameter lang: invalid value $lang: expected lower-case language code")
        }
        val lang3 = if (lang.length == 3) lang else Locale(lang).isO3Language
        this.lang3 = hotfix(iso6392BtoIso6392T(lang3))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || javaClass != other.javaClass) return false
        val language = other as Language?
        return lang3 == language!!.lang3
    }

    override fun hashCode() = lang3.hashCode()

    /**
     * Returns [lang3].
     * @return [lang3]
     */
    override fun toString() = lang3

    override fun compareTo(other: Language) = lang3.compareTo(other.lang3)

    companion object {

        /**
         * @param lang ISO-639-2T three letter code
         * @return ISO-639-2T three letter code
         */
        private fun hotfix(lang: String) = if (lang == "nid") "nld" else lang

        /**
         * JMDict uses ISO-639-2B language code. Normalize to ISO-639-2T.
         * @param lang either ISO-639-2B or ISO-639-2T three letter code.
         * @return ISO-639-2T three letter code
         */
        private fun iso6392BtoIso6392T(lang: String): String {
            val iso6392T = ISO_B_T[lang]
            return iso6392T ?: lang
        }

        /**
         * Maps ISO-639-2B 3-letter lowercase language code to ISO-639-2T 3-letter lowercase language code.
         *
         * Inverse to [ISO_T_B].
         */
        private val ISO_B_T = HashMap<String, String>()

        init {
            ISO_B_T.put("alb", "sqi")
            ISO_B_T.put("arm", "hye")
            ISO_B_T.put("baq", "eus")
            ISO_B_T.put("bur", "mya")
            ISO_B_T.put("chi", "zho")
            ISO_B_T.put("cze", "ces")
            ISO_B_T.put("dut", "nid")
            ISO_B_T.put("fre", "fra")
            ISO_B_T.put("geo", "kat")
            ISO_B_T.put("ger", "deu")
            ISO_B_T.put("gre", "ell")
            ISO_B_T.put("ice", "isl")
            ISO_B_T.put("mac", "mkd")
            ISO_B_T.put("may", "msa")
            ISO_B_T.put("mao", "mri")
            ISO_B_T.put("per", "fas")
            ISO_B_T.put("rum", "ron")
            ISO_B_T.put("slo", "slk")
            ISO_B_T.put("tib", "bod")
            ISO_B_T.put("wel", "cym")
        }

        /**
         * Maps ISO-639-2T 3-letter lowercase language code to ISO-639-2B 3-letter lowercase language code.
         *
         * Inverse to [ISO_B_T].
         */
        private val ISO_T_B = HashMap<String, String>()

        init {
            for ((key, value) in ISO_B_T) {
                ISO_T_B.put(value, key)
            }
        }

        @JvmStatic
        fun create(locale: Locale) = Language(locale.language)

        /**
         * The English language.
         */
        @JvmField val ENGLISH = Language("en")
        /**
         * The Japanese language.
         */
        @JvmField val JAPANESE = Language("jpn")
        /**
         * The German language.
         */
        @JvmField val GERMAN = Language("ger")

        @JvmField val FRENCH = Language("fre")

        @JvmField val RUSSIAN = Language("rus")

        @JvmField val DUTCH = Language("dut")

        @JvmField val PORTUGALESE = Language("por")

        @JvmField val SPANISH = Language("spa")

        @JvmField val HUNGARIAN = Language("hun")

        @JvmField val SLOVENE = Language("slv")

        @JvmField val SWEDISH = Language("swe")
    }
}
