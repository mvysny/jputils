/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Provides check methods.
 *
 * Deprecated: use Kotlin counterparts.
 * @author Martin Vysny
 */
@Deprecated
public class Check {
	private Check() {
		throw new AssertionError();
	}

	@NotNull
	public static <T> T requireNotNull(@Nullable final T value, @NotNull String name) {
        if (value == null) {
            throw new NullPointerException("Value " + name + " is null");
        }
        return value;
    }
	@NotNull
	public static <T> T requireNotNull(@Nullable final T value) {
        if (value == null) {
            throw new NullPointerException();
        }
        return value;
    }

	@NotNull
	public static String requireNotBlank(@Nullable String string) {
        if (MiscUtils.isBlank(string)) {
            throw new IllegalArgumentException("blank");
        }
        return string;
    }

	@NotNull
	public static String requireNotBlank(@NotNull String string, @NotNull String name) {
        if (MiscUtils.isBlank(string)) {
            throw new IllegalArgumentException(name + ": blank");
        }
        return string;
    }

	public static void requireNoBlanks(@NotNull Iterable<? extends String> collection, @NotNull String name) {
		requireNotNull(collection, name);
		for (String s : collection) {
			if (MiscUtils.isBlank(s)) {
				throw new IllegalArgumentException(name + ": collection " + collection + " contains blanks");
			}
		}
	}
}
