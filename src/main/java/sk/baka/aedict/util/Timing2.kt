/*
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util

import org.slf4j.Logger
import java.util.*

/**
 * Measures duration of certain activity and its substeps. Use [timing] to perform the timing.
 */
class Timing2 internal constructor() {
    private val start = System.currentTimeMillis()
    private var currentMarkStart: Long = start
    private var currentMarkName: String? = null

    private data class Mark(val name: String, val duration: Long)

    private val marks = LinkedList<Mark>()

    /**
     * Marks an end of previous part and a beginning of a next part.
     * @param name the next part name, not null
     */
    fun mark(name: String): Timing2 {
        finishMark()
        currentMarkName = name
        return this
    }

    private fun finishMark() {
        if (currentMarkName != null) {
            val newMarkStart = System.currentTimeMillis()
            marks.add(Mark(currentMarkName!!, newMarkStart - currentMarkStart))
            currentMarkStart = newMarkStart
            currentMarkName = null
        }
    }

    /**
     * Finishes the timing measuring and returns total string in form of:
     * 152ms (bootup 100ms, processing 50ms, cleanup 2ms)
     * @return summary string, not null.
     */
    internal fun finish(): String {
        finishMark()
        val sb = marks.joinToString(transform = { "${it.name}: ${it.duration}ms" })
        return "${duration}ms ($sb)"
    }

    /**
     * Return the total timing duration.
     * @return the duration, 0 or greater.
     */
    val duration: Long
        get() = System.currentTimeMillis() - start
}

/**
 * Times given activity; allows sub-activities to be timed separately, using the [Timing2.mark] blocks. Logs results to
 * given logger.
 * @param activity what are we going to do, e.g. "App initialization". Prints out "App initialization took 40ms (foo: 1ms, bar: 25ms)"
 */
fun <R> Logger.timing(activity: String, block: Timing2.() -> R): R {
    val timing = Timing2()
    val result = timing.block()
    info("$activity took ${timing.finish()}")
    return result
}
