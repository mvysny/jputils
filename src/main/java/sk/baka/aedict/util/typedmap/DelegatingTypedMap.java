/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Typed map which delegates all calls to {@link #delegate}. You can use this base class to e.g. filter values,
 * observe value changes etc.
 * @author mvy
 */
public class DelegatingTypedMap extends AbstractTypedMap<DelegatingTypedMap> {
	public DelegatingTypedMap(@NotNull AbstractTypedMap<?> delegate) {
		this.delegate = delegate;
	}

    /**
     * By default delegates all calls to this delegate.
     */
	@NotNull
	public final AbstractTypedMap<?> delegate;

	@NotNull
	@Override
	public Value<Object> get(@NotNull String key) {
		return delegate.get(key);
	}

	@NotNull
	@Override
	protected DelegatingTypedMap put(@NotNull String key, @Nullable Object value) {
		delegate.put(key, value);
		return this;
	}

	@Override
	public boolean contains(@NotNull String key) {
		return delegate.contains(key);
	}

	@Override
	public int size() {
		return delegate.size();
	}

}
