/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.util.typedmap;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import sk.baka.aedict.util.AndroidSparseArray;
import sk.baka.aedict.util.Boxable;
import sk.baka.aedict.util.Boxables;
import sk.baka.aedict.util.Writable;
import sk.baka.aedict.util.Writables;

/**
 * A box of objects of different types. Similar to Android's Bundle.
 * A stable serialization. Serializable/Parcelable format may change, this must not.
 * <p></p>
 * Implementor must not support [Writable], [Boxable] nor Serializable, since
 * that might not be portable from Android to Java and vice versa, or between
 * multiple apps unless care is taken that the class exists on all apps.
 * @author mvy
 */
public final class Box extends AbstractTypedMap<Box> implements Writable, Boxable {

	/**
	 * If true, stores strings as UTF-8 (a bit slower but smaller on the SD Card).
	 * <p></p>
	 * Actually, performance tests shown that storing strings as UTF-16BE is slower
     * than storing them as UTF-8:
	 * <ul>
	 *     <li>s UTF-16BE stringami: 2mb, read time 8301ms, write time 1756ms</li>
	 *     <li>s UTF8 stringami: 1,4mb, read time 3252ms, write time 1361ms</li>
	 * </ul>
	 * Don't therefore use {@link #VAL_STRING2}.
	 */
	public static final boolean USE_OLD_STRINGS = true;

    static final byte VAL_INVALID_TYPE = -10;
    private static final byte VAL_NULL = -1;
	/**
	 * Stores string as UTF-8.
	 */
    private static final byte VAL_STRING = 0;
    private static final byte VAL_INTEGER = 1;
    private static final byte VAL_SHORT = 2;
    private static final byte VAL_BYTE = 3;
    private static final byte VAL_BOX = 4;
    private static final byte VAL_ENUMSET = 5;
    private static final byte VAL_BOOL = 6;
    private static final byte VAL_LIST = 7;
    private static final byte VAL_LIST_OF_BOXES = 8;
    private static final byte VAL_BYTE_ARRAY = 9;
    private static final byte VAL_LONG = 10;
    private static final byte VAL_FLOAT = 11;
    private static final byte VAL_MAP = 13;
    private static final byte VAL_UUID = 14;
	@Deprecated
	private static final byte VAL_STRING2 = 12;

    private static final byte MAX_SIZE = Byte.MAX_VALUE;

    @Override
	public void writeTo(@NotNull DataOutput out) throws IOException {
        out.writeByte(contents.size());
        for (int i = 0; i < contents.size(); i++) {
            out.writeInt(contents.keyAt(i));
            writeObject(contents.valueAt(i), out);
        }
    }

	/**
	 * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
	 * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
	 * @param file the file to write to, not null
	 * @throws IOException on i/o error.
	 */
	@Deprecated
	public void writeTo(@NotNull File file) throws IOException {
		Writables.write(this, file, false);
	}

    /**
     * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
     * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
     * @param file the file to write to, not null
     * @throws IOException on i/o error.
     */
	@Deprecated
    public void writeToGzipped(@NotNull File file) throws IOException {
        Writables.write(this, file, true);
    }

    /**
     * Serializes the box and returns the result as a byte array.
     * @return serialized box, never null.
     */
	@Deprecated
	@NotNull
	public byte[] write() {
		return Writables.write(this);
    }

    static byte getTypeNull(@Nullable Object obj) {
        if (obj == null) {
            return VAL_NULL;
        } else if (obj instanceof String) {
            return USE_OLD_STRINGS ? VAL_STRING : VAL_STRING2;
        } else if (obj instanceof Integer) {
            return VAL_INTEGER;
        } else if (obj instanceof Short) {
            return VAL_SHORT;
        } else if (obj instanceof Byte) {
            return VAL_BYTE;
        } else if (obj instanceof BoxedBox) {
            return VAL_BOX;
        } else if (obj instanceof BoxedEnumSet) {
            return VAL_ENUMSET;
        } else if (obj instanceof Boolean) {
            return VAL_BOOL;
        } else if (obj instanceof Collection) {
            return VAL_LIST;
        } else if (obj instanceof BoxedListOfBoxes) {
            return VAL_LIST_OF_BOXES;
        } else if (obj instanceof BoxedByteArray) {
            return VAL_BYTE_ARRAY;
        } else if (obj instanceof Long) {
            return VAL_LONG;
        } else if (obj instanceof Float) {
            return VAL_FLOAT;
        } else if (obj instanceof Map) {
            return VAL_MAP;
        } else if (obj instanceof UUID) {
            return VAL_UUID;
        } else {
            return VAL_INVALID_TYPE;
        }
    }

    private static byte getType(@Nullable Object obj) {
        final byte result = getTypeNull(obj);
        if (result == VAL_INVALID_TYPE) {
            throw new IllegalStateException("Invalid state: unsupported content type: " + obj.getClass());
        }
        return result;
    }

	private static final Charset FAST_STRING_CH = Charset.forName("UTF-16BE");

    private static void writeObject(@Nullable Object obj, @NotNull DataOutput out) throws IOException {
        final byte type = getType(obj);
        out.writeByte(type);
		if (type == VAL_NULL) {
			return;
		}
		assert obj != null;
        switch (type) {
            case VAL_STRING:
                out.writeUTF((String) obj);
                break;
			case VAL_STRING2:
				// more performant than writeUTF? Not really. Don't use.
				// https://code.google.com/p/aedict/issues/detail?id=426
				final String s = (String) obj;
				final int length = s.length();
				if (length > 65535) {
					throw new RuntimeException("String too long: " + length);
				}
				out.writeShort(length);
				// fastest
				out.write(s.getBytes(FAST_STRING_CH));
				// still not performant enough, takes 0.124ms per invocation, 668ms overall
//				out.writeChars(s);
				// still slower than using Charset
//				for (char c : s.toCharArray()) {
//					out.writeChar(c);
//				}
				break;
            case VAL_INTEGER:
                out.writeInt((Integer) obj);
                break;
            case VAL_SHORT:
                out.writeShort((Short) obj);
                break;
            case VAL_BYTE:
                out.writeByte((Byte) obj);
                break;
            case VAL_BOX:
                ((BoxedBox<?>) obj).writeTo(out);
                break;
            case VAL_ENUMSET:
                ((BoxedEnumSet) obj).writeTo(out);
                break;
            case VAL_BOOL:
                out.writeBoolean((Boolean) obj);
                break;
            case VAL_LIST: {
                final Collection<?> list = (Collection<?>) obj;
                out.writeInt(list.size());
                for (Object o : list) {
                    writeObject(o, out);
                }
            }
            break;
            case VAL_LIST_OF_BOXES:
                ((BoxedListOfBoxes<?>) obj).writeTo(out);
                break;
            case VAL_BYTE_ARRAY: ((BoxedByteArray) obj).writeTo(out);
                break;
            case VAL_LONG:
                out.writeLong((Long) obj);
                break;
            case VAL_FLOAT:
                out.writeFloat((Float) obj);
                break;
            case VAL_MAP: {
                final Map<?, ?> map = (Map<?, ?>) obj;
                out.writeInt(map.size());
                for (Map.Entry<?, ?> entry : map.entrySet()) {
                    writeObject(entry.getKey(), out);
                    writeObject(entry.getValue(), out);
                }
            }
            break;
            case VAL_UUID: {
                final UUID uuid = (UUID) obj;
                out.writeLong(uuid.getLeastSignificantBits());
                out.writeLong(uuid.getMostSignificantBits());
            }
            break;
            default:
                throw new AssertionError("Unimplemented write for " + type);
        }
    }

    static final int MAX_ENUM_LENGTH = Byte.MAX_VALUE;

	@NotNull
	public static Box unbox(@NotNull Box box) {
        return box;
    }

	@Nullable
    private static Object decodeObject(@NotNull DataInput in) throws IOException {
        final byte type = in.readByte();
        switch (type) {
            case VAL_NULL:
                return null;
            case VAL_STRING:
                return in.readUTF();
			case VAL_STRING2: {
				// more performant than readUTF()? Not really - don't use.
				// https://code.google.com/p/aedict/issues/detail?id=426
				final int len = in.readShort() & 0xFFFF;
				final byte[] b = new byte[len * 2];
				in.readFully(b);
				return new String(b, FAST_STRING_CH);
			}
            case VAL_INTEGER:
                return in.readInt();
            case VAL_SHORT:
                return in.readShort();
            case VAL_BYTE:
                return in.readByte();
            case VAL_BOX:
                return BoxedBox.readFrom(in);
            case VAL_ENUMSET:
                return BoxedEnumSet.readFrom(in);
            case VAL_BOOL:
                return in.readBoolean();
            case VAL_LIST: {
                final int len = in.readInt();
                final List<Object> list = new ArrayList<Object>(len);
                for (int i = 0; i < len; i++) {
                    list.add(decodeObject(in));
                }
                return list;
            }
            case VAL_LIST_OF_BOXES:
                return BoxedListOfBoxes.readFrom(in);
            case VAL_BYTE_ARRAY: {
                return BoxedByteArray.readFrom(in);
            }
            case VAL_LONG: return in.readLong();
            case VAL_FLOAT: return in.readFloat();
            case VAL_MAP: {
                final int len = in.readInt();
                final Map<Object, Object> map = new HashMap<Object, Object>(len);
                for (int i = 0; i < len; i++) {
                    map.put(decodeObject(in), decodeObject(in));
                }
                return map;
            }
            case VAL_UUID: {
                final long lsb = in.readLong();
                final long msb = in.readLong();
                return new UUID(msb, lsb);
            }
            default:
                throw new RuntimeException("Unsupported value type:" + type);
        }
    }

	@NotNull
	public static Box readFrom(@NotNull DataInput in) throws IOException {
        final byte size = in.readByte();
        final AndroidSparseArray<Object> content = new AndroidSparseArray<Object>(size);
        for (int i = 0; i < size; i++) {
            final int key = in.readInt();
            content.put(key, decodeObject(in));
        }
        return new Box(content);
    }

	@NotNull
	public static Box readFrom(@NotNull Box box) throws IOException {
		return box;
	}

	@Override
	@NotNull
	public Box box() {
        return this;
    }

	@NotNull
	private final AndroidSparseArray<Object> contents;

    /**
     * Creates a new, empty box.
     */
    public Box() {
        this(new AndroidSparseArray<Object>());
    }

    private Box(@NotNull AndroidSparseArray<Object> contents) {
        this.contents = contents;
    }

	@NotNull
	public Value<Object> get(@NotNull String key) {
        return new Value<Object>(key, contents.get(key.hashCode()));
    }

	@NotNull
    @Override
    protected Box put(@NotNull String key, @Nullable Object value) {
        final int hash = key.hashCode();
        if (value == null) {
            contents.remove(hash);
        } else {
			if (contents.containsKey(hash)) {
				throw new IllegalStateException("Invalid state: " + key + " mapped multiple times, last mapped to " + contents.get(hash));
			}
            if (contents.size() >= MAX_SIZE) {
                throw new IllegalStateException("Invalid state: cannot add another key, max " + MAX_SIZE + " keys allowed");
            }
            contents.put(hash, value);
        }
        return this;
    }

    /**
     * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
     * by the {@code clazz} parameter.
     *
     * @param clazz expected class, not null.
     * @param file the file with the box, not null.
     * @param <T> expected type
     * @return the unboxed object instance, never null.
     */
	@Deprecated
    @NotNull
    public static <T extends Boxable> T unbox(@NotNull Class<T> clazz, @NotNull File file) throws IOException {
		return Boxables.read(clazz, file);
    }

	/**
     * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
     * by the {@code clazz} parameter.
     *
     * @param clazz expected class, not null.
     * @param file the file with the box, not null.
     * @param <T> expected type
     * @return the unboxed object instance, never null.
     */
	@Deprecated
    @NotNull
    public static <T extends Boxable> T unboxGZipped(@NotNull Class<T> clazz, @NotNull File file) throws IOException {
		return Boxables.readGZipped(clazz, file);
    }

    /**
     * Reads the box contents from given box. The object type is not auto-detected: it must instead be provided
     * by the {@code clazz} parameter.
     *
     * @param clazz expected class, not null.
     * @param box the box, not null.
     * @param <T> expected type
     * @return the unboxed object instance, never null.
     */
	@Deprecated
	@NotNull
	public static <T extends Boxable> T unbox(@NotNull Class<T> clazz, @NotNull Box box) {
		return Boxables.read(clazz, box);
    }

	@Nullable
	@Deprecated
	public static <T extends Boxable> T clone(@Nullable T boxable) {
		return Boxables.clone(boxable);
    }

	@Override
    public boolean contains(@NotNull String key) {
        return contents.containsKey(key.hashCode());
    }

	@Override
    public int size() {
        return contents.size();
    }

    @Override
	@NotNull
    public String toString() {
        return contents.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Box box = (Box) o;
        if (!contents.equals(box.contents)) return false;
        return true;
    }

    @Override
    public int hashCode() {
        return contents.hashCode();
    }

    /**
     * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
     * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
     * <p></p>
     * Use {@link #unbox(Class, File)} to read the boxable back.
     * @param boxable the boxable, not null.
     * @param target the file to write to, not null
     * @throws java.io.IOException on i/o error.
     */
	@Deprecated
    public static void write(@NotNull Boxable boxable, @NotNull File target) throws IOException {
		Boxables.write(boxable, target);
    }

    /**
     * Writes this boxable directly to a file. A temp file is first created; after the write is successful,
     * the file is moved over the requested file. This is to protect the original file contents, in case of a write error.
     * <p></p>
     * Use {@link #unboxGZipped(Class, File)} to read the boxable back.
     * @param boxable the boxable, not null.
     * @param target the file to write to, not null
     * @throws java.io.IOException on i/o error.
     */
	@Deprecated
    public static void writeGzipped(@NotNull Boxable boxable, @NotNull File target) throws IOException {
		Boxables.writeGzipped(boxable, target);
    }
}
