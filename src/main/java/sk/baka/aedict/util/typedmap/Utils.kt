/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
@file:Suppress("PLATFORM_CLASS_MAPPED_TO_KOTLIN")

package sk.baka.aedict.util.typedmap

import java.lang.Boolean
import java.lang.Byte
import java.lang.Float
import java.lang.Long
import java.lang.Short
import sk.baka.aedict.util.BitSet
import java.io.Serializable
import kotlin.jvm.internal.ClassBasedDeclarationContainer
import kotlin.reflect.KClass
import kotlin.reflect.KProperty

val KClass<*>.internalJava: Class<*>
    get() = (this as ClassBasedDeclarationContainer).jClass

/**
 * This allows for value classes backed by a typed map. The properties may be specified simply as follows:
 * `var recentEntriesCount: Int by prefs.default(15)`
 */
operator inline fun <reified R: Any?> AbstractTypedMap<*>.getValue(thisRef: Any?, property: KProperty<*>): R {
    val v = get(property.name)
    val clazz = R::class.internalJava
    if (clazz.isEnum) {
        if (!v.isPresent) return null as R
        if (clazz.isInstance(v.value)) return v.value as R
        return clazz.enumConstants[v.byteValue().get().toInt()] as R
    }
    when (R::class) {
        Integer::class -> return v.integer().orNull() as R
        Float::class -> return v.floatValue().orNull() as R
        String::class -> return v.string().orNull() as R
        Short::class -> return v.shortValue().orNull() as R
        Byte::class -> return v.byteValue().orNull() as R
        Boolean::class -> return v.bool().orNull() as R
        Long::class -> return v.longValue().orNull() as R
        ByteArray::class -> return v.byteArray().orNull() as R
        BitSet::class -> return v.bitSet().orNull() as R
    }
    throw RuntimeException("AbstractTypedMap.getValue(): Unsupported type ${R::class}")
}

val KProperty<*>.getterMethodName: String
get() = if (name.startsWith("is")) name else "get${name[0].toUpperCase()}${name.substring(1)}"

operator fun AbstractTypedMap<*>.setValue(thisRef: Any?, property: KProperty<*>, value: Any?) {
    if (value is Enum<*>) {
        if (value.ordinal > Box.MAX_ENUM_LENGTH) {
            throw IllegalArgumentException("Parameter value: invalid value $value: has ordinal of ${value.ordinal}")
        }
        putByte(property.name, value.ordinal.toByte())
    } else {
        putSupportedObject(property.name, value)
    }
}

class DefaultValTypedMap(delegate: AbstractTypedMap<*>, private val defaultValue: Any): DelegatingTypedMap(delegate), Serializable {
    override fun get(key: String): Value<Any> {
        val value = super.get(key)
        return if (value.isPresent) value else Value(key, defaultValue)
    }
}

/**
 * If given key is missing, returns given default value.
 * @param defaultValue the value to return
 */
fun AbstractTypedMap<*>.default(defaultValue: Any) = DefaultValTypedMap(this, defaultValue)
