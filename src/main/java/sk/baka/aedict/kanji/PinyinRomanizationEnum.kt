/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import sk.baka.aedict.dict.Kanjidic2Entry

/**
 * Lists options for pinyin romanization.
 * @author mvy
 */
enum class PinyinRomanizationEnum {
    ToneNumerals {
        override fun getJapaneseOneLiner(entry: Kanjidic2Entry): String = entry.pinyin.joinToString(" ")
    },
    ToneDiacritics {
        override fun getJapaneseOneLiner(entry: Kanjidic2Entry): String =
                entry.parsePinyin().joinToString(" ", transform = { pinyin -> pinyin.readingWithDiacritics })
    };

    /**
     * Vrati pinyin romanizations oddelene medzerou.
     * @param entry kanjidic entry, not null.
     * @return pinyin romanizations oddelene medzerou.
     */
    abstract fun getJapaneseOneLiner(entry: Kanjidic2Entry): String
}
