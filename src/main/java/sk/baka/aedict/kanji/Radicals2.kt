/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

/**
 * The official table of radicals.
 * Jim Breen's KanjiDIC only lists one radical per kanji! Better search by [Parts].
 */
object Radicals2 {
    /**
     * 214 radicals downloaded from view-source:http://nuthatch.com/kanji/demo/radicals.html
     * This is the Jim Breen "classical" / KangXi Zidian table (hopefully ;-)
     */
    val RADICALS = "一个丶丿乙亅二亠人儿入八冂冖冫几凵刀力勹匕匚匚十卜卩厂厶又口囗土士夂夂夕大女子宀寸小尢尸屮山巛工己巾干幺广廴廾弋弓彐彡彳心戈戸手支攴文斗斤方无日日月木欠止歹殳毋比毛氏气水火爪父爻爿片牙牛犬玄玉瓜瓦甘生用田疋疔癶白皮皿目矛矢石示禹禾穴立竹米糸缶网羊羽老而耒耳聿肉臣自至臼舌舛舟艮色艸虍虫血行衣襾見角言谷豆豕豸貝赤走足身車辛辰辷邑酉釆里金長門阜隶隹雨青非面革韋韭音頁風飛食首香馬骨高髟鬥鬯鬲鬼魚鳥鹵鹿麥麻黄黍黒黹黽鼎鼓鼠鼻齊齒龍龜龠"

    private val RADICAL_COUNT = 214

    init {
        require(RADICALS.length == RADICAL_COUNT) { "Shit, some radicals can only be represented with code points" }
    }

    /**
     * Returns the radical.
     * @param radicalNumber radical number, 1..214
     * @return the radical character.
     */
    fun getRadical(radicalNumber: Int): Char {
        require(radicalNumber in 1..RADICAL_COUNT) { "Parameter radicalNumber: invalid value $radicalNumber: must be 1..$RADICAL_COUNT" }
        return RADICALS[radicalNumber - 1]
    }

    /**
     * Returns the radical number, 1..214.
     * @param kanji the kanji, not null
     * @return the radical number or null if given kanji is not a radical character.
     */
    fun getRadicalNumber(kanji: Kanji): Int? {
        if (!kanji.isChar) return null
        val index = RADICALS.indexOf(kanji.toCharacter())
        return if (index < 0) null else index + 1
    }
}
