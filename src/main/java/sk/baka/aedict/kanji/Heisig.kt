/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import sk.baka.aedict.dict.Gloss
import sk.baka.aedict.util.*
import sk.baka.aedict.util.typedmap.Box

import java.io.Serializable
import java.util.HashSet

/**
 * Contains Heisig meta-data for kanji characters. Use the ComputeHeisig class to generate the content of this class.
 * @property keywords Heisig's name of the kanji, e.g. "fish guts" for 乙. Contains english and german translations (EN+GER); may contain multiple senses.
 * @property heisigNumber Heisig Kanji number, the index in Heisig (Remembering The Kanji); L value in kanjidic. For example 71 for 乙. The "Issue 3" number.
 * @property lessonNumber Heisig lesson number, in which the character appeared. For example 5 for 乙. May be null. Allowed range is [LESSON_RANGE], mal by byt 1..56.
 * @property heisigNumber6thEd Heisig Kanji number, the index in Heisig (Remembering The Kanji) 6th edition; L value in kanjidic. For example 71 pre 乙. The 6th edition number; null if not known.
 * @author mvy
 */
data class Heisig(val keywords: Gloss, val heisigNumber: Short, val lessonNumber: Byte?, val heisigNumber6thEd: Int?) : Serializable, Boxable {

    /**
     * Returns the heisig number for given Heisig edition.
     * @param edition Heisig "Remembering The Kanji" edition, 3 or 6.
     * @return Heisig Kanji number or null if not known.
     */
    fun getHeisigNumber(edition: Int) = when(edition) {
        3 -> heisigNumber.toInt()
        6 -> heisigNumber6thEd
        else -> null
    }

    /**
     * Returns all known heisig kanji numbers in all heisig editions
     * @return all known heisig numbers, not null, may be empty.
     */
    val allHeisigNumbers: Set<Int>
        get() {
            val result = HashSet<Int>(2)
            result.add(heisigNumber.toInt())
            if (heisigNumber6thEd != null) {
                result.add(heisigNumber6thEd)
            }
            return result
        }

    override fun box(): Box {
        // "keywords" are deprecated since 23.5.2014, but I can't remove them - what if someone's notepad contains
        // old entries with old Heisig info?
        return Box().putString("keywords", keywords.getAndPreferLang(Language.ENGLISH).joinToString())
                .putBoxable("glossKeywords", keywords)
                .putShort("heisigNumber", heisigNumber)
                .putByte("lessonNumber", lessonNumber)
                .putShort("heisigNumber6thEd", heisigNumber6thEd?.toShort())
    }

    fun getKeywords(lang: Language?) = keywords.getAndPreferLang(lang).joinToString()

    companion object : Unboxable<Heisig> {

        /**
         * Allowed range of heisig lesson numbers, 1..56
         */
        @JvmStatic
        val LESSON_RANGE = 1..56

        /**
         * Returns the kanji quiz for given heisig lesson number.
         * @param lessonNumber lesson number, 1..56 inclusive
         * @return a list of kanjis, never null, should not be empty.
         */
        fun kanjisForLesson(lessonNumber: Int): String {
            if (!LESSON_RANGE.contains(lessonNumber)) {
                throw IllegalArgumentException("Parameter lessonNumber: invalid value $lessonNumber: must be $LESSON_RANGE")
            }
            return map.get(lessonNumber)
        }

        @JvmStatic
        override fun unbox(box: Box): Heisig {
            var keywords = box.get("glossKeywords").boxable(Gloss::class.java).orNull()
            if (keywords == null) {
                val oldEnKeywords = box.get("keywords").string().get()
                keywords = Gloss().add(Language.ENGLISH, oldEnKeywords)
            }
            val heisigNumber6thEd = box.get("heisigNumber6thEd").shortValue().orNull()
            return Heisig(keywords,
                    box.get("heisigNumber").shortValue().get(),
                    box.get("lessonNumber").byteValue().orNull(),
                    heisigNumber6thEd?.toInt())
        }

        // map of heisig lesson to a list of kanjis
        private val map = AndroidSparseArray<String>()

        init {
            map.put(1, "八六十口田日四目月一七三九二五")
            map.put(2, "胃胆冒凸凹古吾呂品唱旦早旭昌明晶朋世亘")
            map.put(3, "自舌千升卓博占旧昇白百朝寸専上下中丸")
            map.put(4, "肌元児凡勺句只員旬的頁頑見首貝貞負万")
            map.put(5, "具刀刃切別則副召可右町昭直有真頂項貢賄工左丁乙乱")
            map.put(6, "兄克女好如子孔母貫了")
            map.put(7, "肖光臭削厚名器省石砂砕硝夕外多大太奇妙小少汐")
            map.put(8, "火灯灰災炎点照煩原吐畑時土圧圭均垣埼順願魚寺封水氷永汁江沖河沼況泉泊泳活消涯川州淡測湖源漁潮")
            map.put(9, "胴里量厘同向埋墨字守安完宣宴宵寄鯉富尚貯洞黒")
            map.put(10, "膜燥苗若苦草葉味薄暦暮相木未末本札朱朴机杏村林枠枯柏株桂案桐墓梢棚森植妹模寛沫漠")
            map.put(11, "然兆先牛特犬状狩猫荻告眺桃洗黙")
            map.put(12, "全金針釣鉢銅銑銘狂茶玉王鎮珠現理合呈界皇柱栓塔宝注主介")
            map.put(13, "逃造連道処前各落略条格額夏客導巡車軌輸辺辻迅迫")
            map.put(14, "週運冗熟冠舎荘吉周景坑塾士壮売夢高鯨涼享京亭軍輝")
            map.put(15, "牧獄攻故敗敬書枚栄覚言訂計討訓詔詠詩詰話語読調談諭諾学警津")
            map.put(16, "成銭茂城域栽桟威試誠賊浅減滅載式弐")
            map.put(17, "肯錠政是堤頻題礎証婿誕止正武歩歴定賦走赴超越渉延建企")
            map.put(18, "遠適炊肺背脂腹冬初制刺芸匕北錦茨猿鐘吹哀商敏敵喝旨昆皆曇雨雲雷霜瞳衣梅裁装壊裏壱製複褐天姉姿橋立章嫡童諮競謁嬌欠次歌毎比資泣海混市布帆渇帝帯帽幅幌幕乞乾滞軟転滴")
            map.put(19, "肪脱凍剖芳鋭荒鏡放方暗盲曽望坊東音培韻境増棟妄妊妨訪説識賠贈亡廷激")
            map.put(20, "燃県染栃歳賓")
            map.put(21, "逐進遂達胞焦腸准独包午唯改虫虹蚕集蛇雌蛍雑曜電地着蝶砲場風確礁奪奮妃観記権許詳嫁竜歓鮮家豚池豪泡洋起差己湯亀準滝羊美羨羽翌習濯")
            map.put(22, "怖思恐恒恩息恵悔悟患悦悼惑惰想愉意感慌慎慕慣憂憎憩憶憾回因団曰困固国園壇磨姻誌認寡泌添串庁床店庫庭麻心必忌忍志忘忙応忠")
            map.put(23, "怒怠怪肢育胎愛允充至致刑我戒爪到手才打払扱鉱批技銃抄投抗抜犠抱抹担拍拐拓拘招拠拡括拾持指茎挑捨獲授掛採接推掲去又及友双反描提叔受揚菜揮台史損吏搭吸携摘摩撃撤操唆支隻雄更看在坂材督板型枝研桑硫硬械棄奨奴妥始設窓存議護室殻寂将没財販治法流浮淑丈乃乳広互軽弁会鼻義返")
            map.put(24, "炭入公出分鉛拙蜜松頒裕訟欲容密谷山岐貧岩沿峠崎崩嵐浴溶込翁")
            map.put(25, "耳聖恥聴職還烈脅脇党慢懐臓臣臨懲列鉄扶力功加劣努披励労拒募勧協掌取葬撮環蔵男敢皮替最瞬架堂堅破行街衡被裂裳夫失規覧姫婆死殉殊残殖寧買賀賛賞波賢趣巨渓常漫潜置罰役彼往征径待律徒従得復微徳徴徹濁迭")
            map.put(26, "透愁利菊菌球和救数類梨奥楼香秀私秋秒委秘様秩称移程税稚稲稼稿穀穂穫誘竹笑笠笹筆等筋筒答策季算箱築簿求米粉粋粒粘粧糧漆迷")
            map.put(27, "個倍倒倣値停健側偵肉傍傑傘悠催傷傾僧儀億償優腐内花化卒荷畝囚柄袋褒符宿貨貸賃丙久府座人仁仏仕他付仙代仮仲件任伏伐休伝伯但位住佐体何佳使例侍依侮便俊俗保信")
            map.put(28, "匁換瓦瓶善喚営融塚夜宮液幣年以弊似併")
            map.put(29, "遅遊炉肩刷戸戻房扇物択勿据掘握啓施旅旋易昼雇堀塀顧訳尺尼尽尿局居屈屋層履沢賜泥涙漏")
            map.put(30, "慰押抽挿捜菓擦由甲申果袖裸示礼社奈祉祝神襟祥祭禁福視課笛款宗宙察尉届岬油崇軸伸")
            map.put(31, "耐逝急逮遭備儒所折録掃君哲唐用画斗料斤斥断昨暫雪曲図曹需析祈科槽訴穏詐婦誓端歯寝尋糖質浄浸両満争事康庸伊漕漸当作群侵近")
            map.put(32, "借遮惜焼憤判券片版芝勝錯半措否畔散藤昔噴暁圏杯墳奔謄巻不渡席之乏度庶廿伴")
            map.put(33, "写務班号族智霧朽柔矛矢知矯誇第汚沸費巧与帰弓弔引弘弟弱強")
            map.put(34, "老考者過煮父効狭拷挟猪著教暑露校棺禍骨髄諸謝孝官管射峡賭足距路跳践踏渚帥渦師躍交身滑較署促追")
            map.put(35, "兵探控搾阪防阿附降陛院陣陥陪陳陽隊階随隔際障隠隣堕墜穴究空突窃窒窪窮窯岳浜深丘")
            map.put(36, "慈懸玄擁蓄畜磁機孫糸系紀約紅納級紛紡索紫累細紳紹終経結絞絡給統絵絹継続維網綿緊総緑緒線締練縁縄縛縦縮繁織繕繰幼幽幾滋弦羅後係")
            map.put(37, "通怨脚腕冷凝興鈴犯勇苑印危却卵卸厄命擬留疑零服柳領宛範貿踊令齢御")
            map.put(38, "退恨酉酌配酒酔酢慨酪酬酵酷酸爵良銀猛猶即鑑喜既限皿盆盗盛盟監朗眼短頭根血塩食飢飯飲飽飾養餓館概娘樹節尊豆豊浪温鼓濫")
            map.put(39, "避胸熱醸凶刈鈍刻劾勢卑収叫菱呼薪新陵陸離睦坪執報核術碑梓壁壌親評該嬢譲宰殺寒糾純希平幸亥辛辞述")
            map.put(40, "性債情憲剰牲割拝勤鋳錘華生産喫嘆星春晴隆難青静睡垂表棒椿奉奏契姓積請謹実害毒寿籍責精峰泰素清縫績乗麦漢漬轄潔俵俸")
            map.put(41, "遷煙腰兼野献南鎌吟含琴陰預栗楠票西要覆標嫌謙予序廉今漂価念")
            map.put(42, "倉候偉違聞悲扉創排問門閉開閑間閣閥閲闘非韓衛欄簡決緯輩潤罪侯快俳")
            map.put(43, "途速瀬倹肝働重刊剣芋勅動勲叙整疎斜薫除険束頼塗衝検種宇汗岸干幹軒余徐")
            map.put(44, "僚抑匠匹区医匿疫疲疾病症痘痛痢痴療癖発登枢欧殴寮廃仰澄迎")
            map.put(45, "恋肥惨膨剤色艶把英率参珍摂文斉斎薬映蚊蛮杉須塁顔変央楽横診対粛紋赤赦絶跡済渋湾黄形彦彩彫彰影修")
            map.put(46, "遣遺無舞勘甘甚旗期某基堪碁棋媒謀欺貴紺")
            map.put(47, "恭選僕共爆助撲異畳阻普暴霊査顕祖業租譜宜殿粗洪組並港繊湿供翼")
            map.put(48, "耕倫遍偏悪典円冊再囲構角解触論講購編井亜溝輪")
            map.put(49, "邦邸郊郎郡部郭郵郷都舗抵捕蒲眠響補婚氏民紙浦底廊低")
            map.put(50, "逓脈舟航般舶船艇艦鍛司后搬瓜嗣盤盾衆飼詞孤段派幻弧伺循")
            map.put(51, "函承呉蒸敷暇益面来革靴衰衷飛声極覇妻娯誤気汽沈繭")
            map.put(52, "耗偽邪為脹釈牙芽番長喪藩雅託髪宅毛審尾展帳張翻")
            map.put(53, "逆逸遇偶悩烏像愚脳免懇戦剛勉鋼挙猟単獣鎖厳援揺蔦嘱晩陶隅暖塑桜墾禅誉媛謡就属象岡鳥鳩鳴島鶏綱鶴巣緩缶弾")
            map.put(54, "能態慮慶熊膚戯劇薦虎虐虚虜虞馬駄駅駆駐駒騎騒験騰驚篤鹿麗")
            map.put(55, "送醜振咲唇関震塊襲娠鬼魂魅魔寅演辰辱農濃")
            map.put(56, "遵錬卯藻癒嚇隷雰朕箇屯巳丑且丹潟罷")
        }
    }
}
