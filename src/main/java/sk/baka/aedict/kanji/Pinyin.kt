/**
 * This file is part of JPUtils.
 *
 * JPUtils is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JPUtils is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */
package sk.baka.aedict.kanji

import org.slf4j.LoggerFactory

import java.io.Serializable
import java.util.*

/**
 * @property reading The reading with no diacritics and no tone numbers. Never null nor blank.
 * @property tone the tone, 0..4
 */
data class Pinyin(val reading: String, val tone: Int) : Serializable {

    @Transient private var _readingWithToneNumber: String? = null

    @Transient private var _readingWithDiacritics: String? = null

    private class Tones(tone1: Char, tone2: Char, tone3: Char, tone4: Char, val neutral: Char) {
        private val tones: List<Char> = Arrays.asList(neutral, tone1, tone2, tone3, tone4)

        fun getTone(toneNumber: Int): Char = tones[if (toneNumber == 5) 0 else toneNumber]

        fun getNumber(s: Char): Int {
            val result = tones.indexOf(s)
            if (result < 0) {
                throw IllegalArgumentException("Parameter s: invalid value $s: not present in $tones")
            }
            return result
        }
    }

    init {
        checkNoDiacritics(reading)
        require(tone in 0..4) { "Parameter tone: invalid value $tone: must be 0..4" }
    }

    override fun toString() = readingWithToneNumber

    /**
     * Returns the reading with tone number, e.g. fang1
     * @return the reading, never blank.
     */
    val readingWithToneNumber: String get() {
        if (_readingWithToneNumber == null) {
            var result = reading
            if (tone > 0) {
                result += tone
            }
            _readingWithToneNumber = result
        }
        return _readingWithToneNumber!!
    }

    /**
     * Returns the reading with diacritics, e.g. fāng
     * @return the reading, never blank.
     */
    val readingWithDiacritics: String get() {
        if (_readingWithDiacritics == null) {
            _readingWithDiacritics = toDiacritics(reading, tone)
        }
        return _readingWithDiacritics!!
    }

    companion object {

        private val TONES_MAP = HashMap<Char, Tones>()
        private val NEUTRAL_MAP = HashMap<Char, Tones>()

        init {
            val tone1 = "āēīōūǖ"
            val tone2 = "áéíóúǘ"
            val tone3 = "ǎěǐǒǔǚ"
            val tone4 = "àèìòùǜ"
            val neutral = "aeiouü"
            val tones: List<Tones> = (0 until tone1.length).map { Tones(tone1[it], tone2[it], tone3[it], tone4[it], neutral[it]) }
            for (tone in tones) {
                NEUTRAL_MAP.put(tone.neutral, tone)
                for (i in 1..4) {
                    TONES_MAP.put(tone.getTone(i), tone)
                }
            }
        }

        private val Char.isNeutral get() = NEUTRAL_MAP[this] != null

        private fun parseDiacritics(reading: String): Pinyin {
            for (i in 0 until reading.length - 1) {
                val c = reading[i]
                val tone = TONES_MAP[c]
                if (tone != null) {
                    return Pinyin(reading.substring(0, i) + tone.neutral + reading.substring(i + 1), tone.getNumber(c))
                }
            }
            return Pinyin(reading, 0)
        }

        /**
         * Parses the pinyin reading. Detects both number tones and diacritics.
         * @param reading the reading, not null, not blank.
         * @return parsed Pinyin
         */
        @Suppress("NAME_SHADOWING")
        fun parse(reading: String): Pinyin {
            var reading = reading
            reading = reading.trim().toLowerCase()
            return when {
                reading.last() in '0'..'4' -> Pinyin(reading.take(reading.length - 1), reading.takeLast(1).toInt())
                reading.last() == '5' -> Pinyin(reading.take(reading.length - 1), 0)
                !containsDiacritics(reading) -> Pinyin(reading, 0)
                else -> parseDiacritics(reading)
            }
        }

        private fun containsDiacritics(reading: String) = reading.any { TONES_MAP.contains(it) }

        private fun checkNoDiacritics(reading: String): String {
            require(reading.isNotBlank())
            require(!containsDiacritics(reading)) { "Parameter reading: invalid value $reading: must not contain diacritics" }
            return reading
        }

        private fun toDiacritics(reading: String, toneNumber: Int, charIndex: Int): String {
            require(toneNumber in 1..4) { "Parameter toneNumber: invalid value $toneNumber: must be 1..4" }
            val t = NEUTRAL_MAP[reading[charIndex]] ?: throw IllegalArgumentException("Parameter reading: invalid value $reading: at pos $charIndex is not vowel")
            return reading.substring(0, charIndex) + t.getTone(toneNumber) + reading.substring(charIndex + 1)
        }

        private fun toDiacritics(reading: String, toneNumber: Int): String {
            if (toneNumber > 0) {
                // rules of algorithm are described here: http://en.wikipedia.org/wiki/Pinyin#Tones
                // and here: http://www.pinyin.info/rules/where.html
                // 1. A and e trump all other vowels and always take the tone mark. There are no Mandarin syllables in Hanyu Pinyin that contain both a and e.
                val aindex = reading.indexOf('a')
                if (aindex >= 0) {
                    return toDiacritics(reading, toneNumber, aindex)
                }
                val eindex = reading.indexOf('e')
                if (eindex >= 0) {
                    return toDiacritics(reading, toneNumber, eindex)
                }
                // 2. In the combination ou, o takes the mark.
                val ouindex = reading.indexOf("ou")
                if (ouindex >= 0) {
                    return toDiacritics(reading, toneNumber, ouindex)
                }
                // 3. In all other cases, the final vowel takes the mark.
                for (i in reading.length - 1 downTo 0) {
                    if (reading[i].isNeutral) {
                        return toDiacritics(reading, toneNumber, i)
                    }
                }
                // wtf? fall back
                log.error("Parameter reading: invalid value $reading: invalid vowel combination")
            }
            return reading
        }

        private val log = LoggerFactory.getLogger(Pinyin::class.java)
    }
}
