[![pipeline status](https://gitlab.com/mvysny/jputils/badges/master/pipeline.svg)](https://gitlab.com/mvysny/jputils/commits/master)

# JP Utils

A set of utility functions for the Japanese language, written in Kotlin (and Java,
but we're moving to Kotlin).

# Usage

The library is in Maven Central, therefore all you need is to add the dependency
`com.gitlab.mvysny.jputils:jputils:2.0` and start using the library.

# Developing

Please feel free to open bug reports to discuss new features; PRs are welcome as well :)

## Building from scratch

Head to the project root and type
```sh
$ ./gradlew
```

That will build the project and run all tests.

## Releasing

To release the library to Maven Central:

1. Edit `build.gradle.kts` and remove `-SNAPSHOT` in the `version=` stanza
2. Commit with the commit message of simply being the version being released, e.g. "1.2.13"
3. git tag the commit with the same tag name as the commit message above, e.g. `1.2.13`
4. `git push`, `git push --tags`
5. Run `./gradlew clean build publish`
6. Continue to the [OSSRH Nexus](https://oss.sonatype.org/#stagingRepositories) and follow the [release procedure](https://central.sonatype.org/pages/releasing-the-deployment.html).
7. Add the `-SNAPSHOT` back to the `version=` while increasing the version to something which will be released in the future,
   e.g. 1.2.14, then commit with the commit message "1.2.14-SNAPSHOT" and push.

## License

GPL 3.0, see [LICENSE](LICENSE)
